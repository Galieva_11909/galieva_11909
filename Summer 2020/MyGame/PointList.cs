﻿using System.Collections;
using System.Drawing;

namespace MyGame
{
    public class Item
    {
        public Point Point;
        public Item Previous;

        public Item()
        {
        }

        public Item(Point point)
        {
            Point = point;
            Previous = null;
        }
    }

    /// <summary>
    ///     Односвзяный список. Каждый элемент своим указателем указывает на предыдущий.
    /// </summary>
    public class PointList
    {
        private Item head;
        private Item tail;

        public int Lenght { get; private set; }

        public PointList(Point headPoint)
        {
            head = new Item(headPoint);
            tail = head;
            Lenght = 1;
        }

        public IEnumerator GetEnumerator()
        {
            var current = tail;
            for (var i = 0; i < Lenght; i++)
            {
                yield return current.Point;
                current = current.Previous;
            }
        }

        /// <summary>
        ///     Добавляет в начало новую точку.
        /// </summary>
        /// <param name="newHead">Новая точка.</param>
        public void AddInTop(Point newHead)
        {
            var newItemBody = new Item
            {
                Point = newHead,
                Previous = null
            };
            head.Previous = newItemBody;
            head = newItemBody;
            Lenght++;
        }

        /// <summary>
        ///     Каждая текущая точка списка становится равна предыдущей.
        /// </summary>
        /// <param name="newHeadLocation">Точка, которой становится равна "голова" списка. </param>
        public void UpdatePoints(Point newHeadLocation)
        {
            AddInTop(newHeadLocation);
            DeleteTail();
        }

        /// <summary>
        ///     Удаляет последний эделемент списка.
        /// </summary>
        private void DeleteTail()
        {
            tail = tail.Previous;
            Lenght--;
        }
    }
}