﻿using System;
using System.Drawing;

namespace MyGame
{
    public class Apple
    {
        private readonly Random rnd = new Random();

        public Point Location { get; private set; }

        private Image image { get; }

        public Apple()
        {
            image = Images.Apple;
        }

        /// <summary>
        ///     Перемещает яблоко в рандомное место на карте.
        /// </summary>
        /// <param name="map">Карта, на которой перемещают яблоко. </param>
        public void GenerateLocation(Map map)
        {
            var x = rnd.Next(0, map.Weight);
            var y = rnd.Next(0, map.Height);
            Location = new Point(x, y);
        }
    }
}