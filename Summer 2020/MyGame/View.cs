﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace MyGame
{
    public partial class View : UserControl
    {
        private readonly GameModeManager game;
        private readonly int cellSize;

        public View(GameModeManager gameModeManager) // Передаю ссылу на GameModel
        {
            InitializeComponent();
            DoubleBuffered = true;
            game = gameModeManager;
            cellSize = 40;
            Size = new Size(game.Map.Weight * cellSize, game.Map.Height * cellSize);
            BackColor = Color.LightGreen;
        }

        private void View_Load(object sender, EventArgs e)
        {
        }

        private void View_Paint(object sender, PaintEventArgs e)
        {
            var appleLocation = new Point(game.Apple.Location.X * cellSize, game.Apple.Location.Y * cellSize);
            e.Graphics.DrawImage(Images.Apple, appleLocation.X, appleLocation.Y, cellSize, cellSize);
            var counterOfLength = 1;
            var snakeHead = Images.Down;
            switch (game.Snake.Direction)
            {
                case Direction.Down:
                    snakeHead = Images.Down;
                    break;
                case Direction.Up:
                    snakeHead = Images.Up;
                    break;
                case Direction.Right:
                    snakeHead = Images.Right;
                    break;
                case Direction.Left:
                    snakeHead = Images.Left;
                    break;
            }

            foreach (var bodyItemLocation in game.Snake.BodyWithHead)
            {
                var pointLocation = (Point) bodyItemLocation;
                var currentLocation = new Point(pointLocation.X * cellSize, pointLocation.Y * cellSize);
                if (counterOfLength == game.Snake.BodyWithHead.Lenght)
                {
                    e.Graphics.DrawImage(snakeHead, currentLocation.X, currentLocation.Y, cellSize, cellSize);
                }
                else
                {
                    e.Graphics.DrawImage(Images.circle, currentLocation.X, currentLocation.Y, cellSize, cellSize);
                }

                counterOfLength++;
            }
        }
    }
}