﻿using System.Drawing;

namespace MyGame
{
    public enum Direction
    {
        Left,
        Right,
        Up,
        Down
    }

    public class Snake
    {
        /// <summary>
        ///     Змея сыта? True - только что съела яблоко, false - в противном случае.
        /// </summary>
        public bool IsFed;
        /// <summary>
        ///     Тело змеи, включая голову, представленное в виде односвзяного списка.
        /// </summary>
        public PointList BodyWithHead;
        /// <summary>
        ///     Расположение головы змеи.
        /// </summary>
        public Point HeadLocation;
        /// <summary>
        ///     Шаг змеи.
        /// </summary>
        public int Step;
        /// <summary>
        ///     Направление движения.
        /// </summary>
        public Direction Direction;

        /// <summary>
        ///     Создает будущее тело змеи (Body), голова которой в данной точке находится.
        /// </summary>
        /// <param name="headLocation"> Расположение головы. </param>
        public Snake(Point headLocation)
        {
            HeadLocation = headLocation;
            BodyWithHead = new PointList(headLocation);
        }

        public void MoveDown()
        {
            HeadLocation = new Point(HeadLocation.X, HeadLocation.Y + Step);
            // Если змея только что поела...
            if (IsFed)
            {
                // то она должна вырасти и снова быть голодной.
                BodyWithHead.AddInTop(HeadLocation);
                IsFed = false;
            }
            else
            {
                BodyWithHead.UpdatePoints(HeadLocation);
            }

            Direction = Direction.Down;
        }

        public void MoveUp()
        {
            HeadLocation = new Point(HeadLocation.X, HeadLocation.Y - Step);
            // Если змея только что поела...
            if (IsFed)
            {
                // то она должна вырасти и снова быть голодной.
                BodyWithHead.AddInTop(HeadLocation);
                IsFed = false;
            }
            else
            {
                BodyWithHead.UpdatePoints(HeadLocation);
            }

            Direction = Direction.Up;
        }

        public void MoveRight()
        {
            HeadLocation = new Point(HeadLocation.X + Step, HeadLocation.Y);
            // Если змея только что поела...
            if (IsFed)
            {
                // то она должна вырасти и снова быть голодной.
                BodyWithHead.AddInTop(HeadLocation);
                IsFed = false;
            }
            else
            {
                BodyWithHead.UpdatePoints(HeadLocation);
            }

            Direction = Direction.Right;
        }

        public void MoveLeft()
        {
            HeadLocation = new Point(HeadLocation.X - Step, HeadLocation.Y);
            // Если змея только что поела...
            if (IsFed)
            {
                // то она должна вырасти и снова быть голодной.
                BodyWithHead.AddInTop(HeadLocation);
                IsFed = false;
            }
            else
            {
                BodyWithHead.UpdatePoints(HeadLocation);
            }

            Direction = Direction.Left;
        }

        /// <summary>
        ///     Проверяет, находится ли змея в данной точке.
        /// </summary>
        /// <param name="point"> Точка для проверки. </param>
        /// <returns></returns>
        public bool Contains(Point point)
        {
            var length = 1;
            foreach (var bodyItem in BodyWithHead)
            {
                if ((Point) bodyItem == point && length != BodyWithHead.Lenght)
                    return true;
                length++;
            }

            return false;
        }
    }
}