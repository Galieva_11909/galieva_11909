﻿namespace MyGame
{
    public class Map
    {
        public int Weight;
        public int Height;

        public Map(int weight, int height)
        {
            Weight = weight;
            Height = height;
        }
    }
}