﻿using System.Drawing;
using System.IO;
using Newtonsoft.Json;

namespace MyGame
{
    public enum GameState
    {
        Go,
        /// <summary>
        ///     // Процесс поедания. Нужно для Controller, чтобы считать очки.
        /// </summary>
        Process,
        Stop
    }

    public class GameModeManager
    {
        public Snake Snake;
        public Apple Apple;
        public Map Map;

        /// <summary>
        ///     Состояние игры.
        /// </summary>
        public GameState GameState { get; private set; }

        public GameModeManager()
        {
            // Получаю размеры карты
            Map = JsonConvert.DeserializeObject<Map>(File.ReadAllText("GameData.json"));
            Map = new Map(10, 10);

            // Змею помещаю в середину карты.
            Snake = new Snake(new Point(Map.Weight / 2, Map.Height / 2))
            {
                Step = 1,
                // Начальное движение вниз. 
                Direction = Direction.Down
            };
            GameState = GameState.Go;
            Apple = new Apple();
            // Генерирую в рандомном месте яблоко.
            Apple.GenerateLocation(Map);
        }

        /// <summary>
        ///     Запускает игру,перемещая змею в зависимости от того, на какую сторону она движится. При этом змея, выходя с одной
        ///     стороны, появляется с другой.
        /// </summary>
        public void Play()
        {
            if (GameState == GameState.Stop) return;
            GameState = GameState.Go;
            switch (Snake.Direction)
            {
                case Direction.Down:
                    if (Snake.HeadLocation.Y == Map.Height - 1)
                        Snake.HeadLocation = new Point(Snake.HeadLocation.X, -1);
                    Snake.MoveDown();
                    break;
                case Direction.Up:
                    if (Snake.HeadLocation.Y == 0)
                        Snake.HeadLocation = new Point(Snake.HeadLocation.X, Map.Height);
                    Snake.MoveUp();
                    break;
                case Direction.Right:
                    if (Snake.HeadLocation.X == Map.Weight - 1)
                        Snake.HeadLocation = new Point(-1, Snake.HeadLocation.Y);
                    Snake.MoveRight();
                    break;
                case Direction.Left:
                    if (Snake.HeadLocation.X == 0)
                        Snake.HeadLocation = new Point(Map.Weight, Snake.HeadLocation.Y);
                    Snake.MoveLeft();
                    break;
            }

            // Проверяем, не получилось ли так, что на клетке, на которую переместилась змея, стоит фрукт или змея ударилась о свой хвост.
            CheckCurrentCell();
        }

        public void TurnSnakeRight()
        {
            // Условие, чтобы нельзя ыбло поверуть на 180 градусов.
            if (Snake.Direction != Direction.Left)
                Snake.Direction = Direction.Right;
        }

        public void TurnSnakeLeft()
        {
            // Условие, чтобы нельзя ыбло поверуть на 180 градусов.
            if (Snake.Direction != Direction.Right)
                Snake.Direction = Direction.Left;
        }

        public void TurnSnakeUp()
        {
            // Условие, чтобы нельзя ыбло поверуть на 180 градусов.
            if (Snake.Direction != Direction.Down)
                Snake.Direction = Direction.Up;
        }

        public void TurnSnakeDown()
        {
            // Условие, чтобы нельзя ыбло поверуть на 180 градусов.
            if (Snake.Direction != Direction.Up)
                Snake.Direction = Direction.Down;
        }

        /// <summary>
        ///     Проверяет наличие фрукта и хвоста змеи в текущей клетки и меняет состояние игры в зависимости от результата
        ///     проверки.
        /// </summary>
        private void CheckCurrentCell()
        {
            // Если на этой клетке яблоко...
            if (Snake.HeadLocation == Apple.Location)
            {
                // То змея становится сытой...
                Snake.IsFed = true;
                // Яблоко перемещается на другоую позицию.
                Apple.GenerateLocation(Map);
                // Если так получилось, что яблоко сгенерировалось в том месте, где находится змея в текущий момент...
                if (Snake.Contains(Apple.Location))
                    // То до тех пор, пока ситуация не поменяется, перемещаем яблоко.
                    while (Snake.Contains(Apple.Location))
                    {
                        Apple.GenerateLocation(Map);
                    }

                // Происходит процесс поедания.
                GameState = GameState.Process;
            }
            // Иначе если змея ударилась о хвост...
            else if (Snake.Contains(Snake.HeadLocation))
            {
                // Игра останавливается.
                GameState = GameState.Stop;
            }
        }
    }
}