﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace MyGame
{
    public partial class ControllerManeForm : Form
    {
        private readonly View view;
        private readonly GameModeManager gameModeManagerGame;
        private readonly Timer timer;
        // Очки игрока.
        private readonly Label pointsLable;
        private int points;

        public ControllerManeForm()
        {
            InitializeComponent();

            // Подключаю логику и визуал к контрллеру.
            gameModeManagerGame = new GameModeManager();
            view = new View(gameModeManagerGame);
            Size = new Size(view.Size.Width + 150, view.Size.Height + 150);
            Controls.Add(view);
            pointsLable = new Label
            {
                Location = new Point(view.Size.Width + 50, 50),
                Size = new Size(50, 50),
                Text = $"Points: {points}"
            };
            Controls.Add(pointsLable);
            timer = new Timer();
            timer.Interval = 800;
            timer.Tick += update;
            timer.Start();
        }

        private void update(object sender, EventArgs e)
        {
            gameModeManagerGame.Play();
            view.Invalidate();
            if (gameModeManagerGame.GameState == GameState.Process)
            {
                timer.Interval = timer.Interval >= 100 ? timer.Interval -= 50 : 100;
                points++;
                pointsLable.Text = $"Points: {points}";
            }
            else if (gameModeManagerGame.GameState == GameState.Stop)
            {
                timer.Stop();
                ErrorMessage();
            }
        }

        private void ErrorMessage()
        {
            var result = MessageBox.Show(
                $"Points: {points}",
                "Game over!",
                MessageBoxButtons.OK,
                MessageBoxIcon.None,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.RightAlign);
            if (result == DialogResult.OK)
            {
                Close();
            }
        }

        private void turnSnake(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Right:
                    gameModeManagerGame.TurnSnakeRight();
                    break;
                case Keys.Left:
                    gameModeManagerGame.TurnSnakeLeft();
                    break;
                case Keys.Down:
                    gameModeManagerGame.TurnSnakeDown();
                    break;
                case Keys.Up:
                    gameModeManagerGame.TurnSnakeUp();
                    break;
            }
        }

        private void ControllerManeForm_KeyDown(object sender, KeyEventArgs e)
        {
            turnSnake(e);
        }

        private void ControllerManeForm_Load(object sender, EventArgs e)
        {
        }
    }
}