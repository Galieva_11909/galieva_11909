﻿namespace MyGame
{
    partial class ControllerManeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ControllerManeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(710, 450);
            this.KeyPreview = true;
            this.Name = "ControllerManeForm";
            this.Text = "Snake";
            this.Load += new System.EventHandler(this.ControllerManeForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ControllerManeForm_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion
    }
}

