﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;


namespace Sem_1
{
    [TestFixture]
    public class TestsInsert
    {
        [Test]
        public void OrdinaryInsert()
        {
            var actualPolinom = new Polinom();
            actualPolinom.Build("BuildAPolinom1.txt");
            actualPolinom.Insert(1000, 6, 9, 12);
            var expectedPolinom = new Polinom();
            expectedPolinom.Build("Insert1.txt");
            Assert.AreEqual(expectedPolinom, actualPolinom);
        }

        [Test]
        public void InsertWithCreatingANewElementInTheEnd()
        {
            var actualPolinom = new Polinom();
            actualPolinom.Build("BuildAPolinom1.txt");
            actualPolinom.Insert(2, 50, 60, 70);
            var expectedPolinom = new Polinom();
            expectedPolinom.Build("BuildAPolinom1.txt");
            var addVector = new Vector() { X = 70, Y = 60, Z = 50 };
            var addElement = new Element() { K = 2, VectorOfDegrees = addVector };
            expectedPolinom.Add(addElement);
            Assert.AreEqual(expectedPolinom, actualPolinom);
        }

        [Test]
        public void InsertWithCreatingANewElementInTheMiddle()
        {
            var actualPolinom = new Polinom();
            actualPolinom.Build("BuildAPolinom1.txt");
            actualPolinom.Insert(30, 5, 10, 11);
            var expectedPolinom = new Polinom();
            expectedPolinom.Build("Insert2.txt");
            Assert.AreEqual(expectedPolinom, actualPolinom);
        }

        [Test]
        public void InsertWithCreatingInTheFirstIteration()
        {
            var actualPolinom = new Polinom();
            actualPolinom.Build("BuildAPolinom1.txt");
            actualPolinom.Insert(5 ,0, 1, 0);
            var expectedPolinom = new Polinom();
            expectedPolinom.Build("Insert3.txt");
            Assert.AreEqual(expectedPolinom, actualPolinom);
        }
    }
}
