﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace Sem_1
{
    [TestFixture]
    public class TestsGetResult
    {
        [Test]
        public void ResultWithPositivePoint()
        {
            var polinom = new Polinom();
            polinom.Build("BuildAPolinom1.txt");
            Assert.AreEqual(13514980.0, polinom.GetResult(2, 1, 1));
        }

        [Test]
        public void ResultWithZero()
        {
            var polinom = new Polinom();
            polinom.Build("GetResult1.txt");
            Assert.AreEqual(5, polinom.GetResult(0, 1, 1));
        }


    }
}
