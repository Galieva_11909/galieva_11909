﻿using System;
using System.IO;
using System.Text;

namespace Sem_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Работа каждого метода должна быть продемонстрирована в методе main, с выводом результата работы каждого метода на экран.

            var polinom = new Polinom();

            Console.WriteLine("Работа метода декодирования при передаче 0:");
            polinom.Build("Program1.txt");
            RestorePolinom(0, polinom);

            Console.WriteLine("Работа метода декодирования при передаче 1:");
            polinom.Build("Program1.txt");
            RestorePolinom(1, polinom);

            Console.WriteLine("Работа метода вставки, когда подобного элемента не существует: ");
            Insert(0, polinom);

            Console.WriteLine("Работа метода вставки, когда подобный элемент существует: ");
            Insert(1, polinom);

            Console.WriteLine("Работа метода удаления элемента:");
            Delete(polinom);

            Console.WriteLine("Сложение двух полиномов:");
            Sum(polinom);

            Console.WriteLine("Взятие производной по переменной:");
            GetDerivative(polinom);

            Console.WriteLine("Нахождение вектора степеней одночлена, имеющего минимальный коэффициент:");
            Min(polinom);

            Console.WriteLine("Значение многочлена в точке:");
            GetResult(polinom);
        }
        public static void RestorePolinom(int flag, Polinom polinom)
        {
            if (flag == 1)
                polinom.RestorePolinom(1, "Program0.txt");
            else
                polinom.RestorePolinom(0, "Program0.txt");
            var data = File.ReadAllText("Program0.txt");
            Console.WriteLine(data + "\n");
        }

        public static void Insert(int flag, Polinom polinom)
        {
            polinom.Build("Program1.txt");
            if (flag == 0)
                polinom.Insert(6666, 7, 8, 5);
            else
                polinom.Insert(6666, 10, 11, 12);
            RestorePolinom(1, polinom);
        }

        public static void Delete(Polinom polinom)
        {
            polinom.Build("Program1.txt");
            polinom.Delete(7, 8, 9);
            RestorePolinom(1, polinom);
        }

        public static void Sum(Polinom polinom)
        {
            polinom.Build("Program1.txt");
            var polinom2 = new Polinom();
            polinom2.Build("Program2.txt");
            //Результирующий полином.
            var sum = new Polinom();
            sum = polinom + polinom2;
            Console.WriteLine("Первый полином:");
            RestorePolinom(1, polinom);
            Console.WriteLine("Второй полином:");
            RestorePolinom(1, polinom2);
            Console.WriteLine("Сумма:");
            RestorePolinom(1, sum);
        }

        public static void GetDerivative(Polinom polinom)
        {
            Console.WriteLine("Введите переменную, по которой взять производную:");
            var value = Console.ReadLine();
            polinom.Build("Program1.txt");
            var result = polinom.GetDerivative(value);
            Console.WriteLine("Исходный полином:");
            RestorePolinom(1, polinom);
            Console.WriteLine("Производная по {0}:", value);
            RestorePolinom(1, result);
        }

        public static void Min(Polinom polinom)
        {
            polinom.Build("Program1.txt");
            var min = polinom.Min();
            Console.WriteLine("Z = {0}, Y = {1}, X = {2}\n", min.Z, min.Y, min.X);
        }

        public static void GetResult(Polinom polinom)
        {
            Console.WriteLine("Введите z, y, x через пробел - точку, в которой нужно найти значение полинома:");
            var point = Console.ReadLine().Split(' ');
            polinom.Build("Program3.txt");
            var number = polinom.GetResult(double.Parse(point[0]), double.Parse(point[1]), double.Parse(point[2]));
            Console.WriteLine("Исходный полином:");
            RestorePolinom(0, polinom);
            Console.WriteLine("Значение полинома в точке Z = {0}, Y = {1}, X = {2} равно {3}", point[0], point[1], point[2], number);
        }
    }
}
