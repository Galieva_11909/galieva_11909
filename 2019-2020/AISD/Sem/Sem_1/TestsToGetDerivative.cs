﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
namespace Sem_1
{
    [TestFixture]
    public class TestsToGetDerivative
    {
        [Test]
        public void GetDerivativeWithNonexcistentLetter()
        {
            var polinom = new Polinom();
            polinom.Build("BuildAPolinom1.txt");
            var actual = Assert.Throws<FormatException>(() => polinom.GetDerivative("v"));
            Assert.That(actual.Message, Is.EqualTo("Нет такой переменной"));
        }

        [Test]
        public void GetDerivativeX()
        {
            var polinom = new Polinom();
            polinom.Build("BuildAPolinom1.txt");      
            var expected = new Polinom();
            expected.Build("GetDerivative1.txt");        
            Assert.AreEqual(expected, polinom.GetDerivative("x"));
        }
        
        [Test]
        public void GetDerivativeY()
        {
            var polinom = new Polinom();
            polinom.Build("BuildAPolinom1.txt");
            var expected = new Polinom();
            expected.Build("GetDerivative2.txt");
            Assert.AreEqual(expected, polinom.GetDerivative("y"));
        }

        [Test]
        public void GetDerivativeZ()
        {
            var polinom = new Polinom();
            polinom.Build("BuildAPolinom1.txt");
            var expected = new Polinom();
            expected.Build("GetDerivative3.txt");
            Assert.AreEqual(expected, polinom.GetDerivative("z"));
        }
    }
}
