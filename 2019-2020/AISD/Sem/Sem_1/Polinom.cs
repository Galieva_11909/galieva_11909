﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sem_1
{
    /// <summary>
    /// Вектор показателей степеней.
    /// </summary>
    public class Vector
    {
        public int Z;
        public int Y;
        public int X;
        public override bool Equals(object obj)
        {
            if (!(obj is Vector)) return false;
            var vector = obj as Vector;
            return (vector.X ==X && vector.Y == Y && vector.Z == Z);
        }
    }
    /// <summary>
    /// Элемент списка.
    /// </summary>
    public class Element
    {
        /// <summary>
        /// Коэффициент.
        /// </summary>
        public int K;
        /// <summary>
        ///  VectorOfDegrees - вектор показателей степеней при коэффициенте K.
        /// </summary>
        public Vector VectorOfDegrees;
        public Element Next;
    }

    /// <summary>
    /// Полином от трех переменных.
    /// </summary>
    public class Polinom
    {
        private Element head;
        private Element tail;
        /// <summary>
        /// Количество векторов показателей степеней (одночленов) в данном полиноме.
        /// </summary>
        public int Count { get; private set; }

        public override bool Equals(object obj)
        {
            if (!(obj is Polinom)) return false;
            var polinom = obj as Polinom;
            if (polinom.Count != Count) return false;
            var flag = true;
            var currentElement1 = head;
            var currentElement2 = polinom.head;
            for(int i=0; i<Count; i++)
            {
                flag = (currentElement1.K == currentElement2.K
                    && currentElement1.VectorOfDegrees.X == currentElement2.VectorOfDegrees.X
                    && currentElement1.VectorOfDegrees.Y == currentElement2.VectorOfDegrees.Y
                    && currentElement1.VectorOfDegrees.Z == currentElement2.VectorOfDegrees.Z);
                currentElement1 = currentElement1.Next;
                currentElement2 = currentElement2.Next;
            }
            return flag;
        }

        /// <summary>
        /// Добавляет элемент в полином.
        /// </summary>
        /// <param name="current">Добавляемый элемент.</param>
        public void Add(Element current)
        {
            if (head == null)
            {
                head = current;
                tail = current;
            }
            else
            {
                tail.Next = current;
                tail = current;
            }
            this.Count++;
        }

        /// <summary>
        /// Строит список по полиному, заданному массивом коэффициентов в некотором текстовом файле.
        /// </summary>
        /// <param name="filenName">Название файла, в котором содержится массив коэффициентов.</param>
        /// <returns></returns>        
        public Polinom Build(string filenName)
        {
            var values = ToGetValues(filenName);           
            for (int i = 0; i < values.Length; i++)
            {
                var currentVector = new Vector() { Z = values[i][1], Y = values[i][2], X = values[i][3] };
                var current = new Element() { K = values[i][0], VectorOfDegrees = currentVector, Next = null };
                this.Add(current);
            }
            return this;
        }
        /// <summary>
        /// Получает значения коэффициентов из текстового файла и "распределяет" их по одночленам, представив в виде массива массивов.
        /// </summary>
        /// <param name="filenName">Текстовый файл со значениями коэффициентов.</param>
        /// <returns></returns>
        private static int[][] ToGetValues(string filenName)
        {
            var data = (File.ReadAllText(filenName)).Split("\n");
            if (data[0] == "")
                throw new FormatException("Пустой файл");
            var values = new int[data.Length][];
            for (int i = 0; i < data.Length; i++)
            {
                values[i] = new int[4];
                var current = data[i].Split(' ');
                if (current[0] != "" && current[0] != "\r" && current[0] != "\n")
                {
                    for (int j = 0; j < 4; j++)
                    {
                        values[i][j] = int.Parse(current[j]);
                    }
                }
                else
                    break;
            }
            return values;
        }

        /// <summary>
        /// Восстанавливает исходный вид полинома с выводом результата в текстовый файл и освобождением выделенной динамической памяти.
        /// </summary>
        /// <param name="flag">0 - представить в виде суммы одночленов; 1 - представить в виде массива коэффициентов.</param>
        /// <param name="fileName">Название файла, в который выведется результат </param>
        public void RestorePolinom(int flag, string fileName)
        {
            if (flag != 0 && flag != 1)
            {
                throw new FormatException("Неверный параметр");
            }

            // Данные, которые будут в файле.
            var data = new StringBuilder();
            var currentElement = head;
            for (int i = 0; i < Count; i++)
            {
                if (flag == 0)
                {
                    data.Append(currentElement.K + "*z^" + currentElement.VectorOfDegrees.Z + "*y^" + currentElement.VectorOfDegrees.Y + "*x^" + currentElement.VectorOfDegrees.X);
                    if (i != Count - 1)
                        data.Append("+");
                }
                else
                {
                    data.Append(currentElement.K + " " + currentElement.VectorOfDegrees.Z + " " + currentElement.VectorOfDegrees.Y + " " + currentElement.VectorOfDegrees.X + "\n");
                }
                currentElement = currentElement.Next;
            }
            //Освобождаем память.
            head = null;
            tail = null;
            Count = 0;
            File.WriteAllText(fileName, data.ToString());
        }

        /// <summary>
        /// Вставляет элемента в некоторую позицию списка. Если текущий элемент существует, то заменяет его.
        /// </summary>
        /// <param name="k">Коэффициент.</param>
        /// <param name="z">Переменная Z.</param>
        /// <param name="y">Переменная Y.</param>
        /// <param name="x">Переменная X</param>
        public void Insert(int k, int z, int y, int x)
        {
            var currentElement = head;
            Element previousElement = null;
            for (int i = 0; i < Count; i++)
            {
                // Если степени совпадают, то все ок, просто меняем значение коэффициента.
                if (currentElement.VectorOfDegrees.Z == z && currentElement.VectorOfDegrees.Y == y && currentElement.VectorOfDegrees.X == x)
                {
                    currentElement.K = k;
                    break;
                }
                // Если текущий вектор меньше...
                else if (currentElement.VectorOfDegrees.Z < z || currentElement.VectorOfDegrees.Y < y || currentElement.VectorOfDegrees.X < x)
                {
                    // И элемент не последний...
                    if (i != Count - 1)
                    {
                        // То переходим к следующему элементу.
                        previousElement = currentElement;
                        currentElement = currentElement.Next;
                    }

                    // Если элемент последний...
                    else
                    {
                        // То создаем новый элемент и меняем показатель для tail.
                        Count++;
                        var newVector = new Vector() { X = x, Y = y, Z = z };
                        var newElement = new Element() { K = k, VectorOfDegrees = newVector, Next = currentElement };
                        currentElement.Next = newElement;
                        tail = newElement;
                        break;
                    }
                }
                // Если текущее число больше, то элемента с такими показателями степени не существует, значит, нужно его создать.
                else
                {
                    Count++;
                    var newVector = new Vector() { X = x, Y = y, Z = z };
                    var newElement = new Element() { K = k, VectorOfDegrees = newVector, Next = currentElement };
                    // Если это происходит на первой итерации...
                    if (i == 0)
                    {
                        // то меняем показатель head.
                        newElement.Next = head;
                        head = newElement;
                        break;
                    }
                    // Иначе меняем показатель предыдущего элемента.
                    previousElement.Next = newElement;
                    break;
                }
            }
        }

        /// <summary>
        /// Для данного показателей степеней удаляет соотвутствующий элемент из списка.
        /// </summary>
        /// <param name="z"></param>
        /// <param name="y"></param>
        /// <param name="x"></param>
        public void Delete(int z, int y, int x)
        {
            var currentElement = head;
            Element previousElement = null;
            for (int i = 0; i < Count; i++)
            {
                // Если элемент найден..
                if (currentElement.VectorOfDegrees.Z == z && currentElement.VectorOfDegrees.Y == y && currentElement.VectorOfDegrees.X == x)
                {
                    // И если он первый, то меняем указатель head.
                    if (i == 0)
                    {
                        head = head.Next;
                    }
                    // Если он последний, то меняем указатель tail.
                    else if (i == Count - 1)
                    {
                        tail = previousElement;
                        tail.Next = null;
                    }
                    // Иначе меняем указатель предыдущего элемента на следующий
                    else
                    {
                        previousElement.Next = currentElement.Next;
                    }
                    Count--;
                    break;
                }
                // Если элемент не найден, то запоминаем текущий элемент и переходим к следующему за ним.
                else if (i != Count - 1)
                {
                    previousElement = currentElement;
                    currentElement = currentElement.Next;
                }
                // Если это последняя итерация цикла, то переданного элемента в полиноме не существует
                else
                {
                    throw new FormatException("Элемента с такими показателями не существует");
                }
            }
        }

        /// <summary>
        /// Строит полином, являющийся частной производной исходного полинома по одному из его переменных.
        /// </summary>
        /// <param name="letter">Переменная, по которой считается производная.</param>
        /// <returns></returns>
        public Polinom GetDerivative(string letter)
        {
            // Степень переменной, по которой берется производная
            var degree = 0;
            var currentElement = head;
            // Результирующий полином.
            var result = new Polinom();          
            letter.ToLower();
            for (int i = 0; i < Count; i++)
            {
                // Добавляемый элемент.
                var add = new Element();
                var vector = new Vector();
                // Находим degree - cтепень переменной текущего одночлена и вычитаем из него 1 (по правилу нахождения производной от переменной в n-ой степени)
                switch (letter)
                {
                    case "x":
                        vector.X = currentElement.VectorOfDegrees.X - 1;
                        degree = currentElement.VectorOfDegrees.X;
                        break;
                    case "y":
                        vector.Y = currentElement.VectorOfDegrees.Y - 1;
                        degree = currentElement.VectorOfDegrees.Y;
                        break;
                    case "z":
                        vector.Z = currentElement.VectorOfDegrees.Z - 1;
                        degree = currentElement.VectorOfDegrees.Z;
                        break;
                    default:
                        throw new FormatException("Нет такой переменной");
                }
                if (letter != "x")
                {
                    vector.X = currentElement.VectorOfDegrees.X;
                }
                if (letter != "y")
                {
                    vector.Y = currentElement.VectorOfDegrees.Y;
                }
                if (letter != "z")
                {
                    vector.Z = currentElement.VectorOfDegrees.Z;
                }
                add.VectorOfDegrees = vector;
                add.K = currentElement.K * degree;
                result.Add(add);
                result.Count = Count;
                currentElement = currentElement.Next;
            }
            return result;
        }

        /// <summary>
        /// Начиная с текущего элемента некоторого полинома, добавляет все его элементы в sum.
        /// </summary>
        /// <param name="addedElement">Текущий элемент некоторого полинома.</param>
        /// <param name="sum">Результирующий полином.</param>
        private static void AddMod(Element addedElement, Polinom sum)
        {
            while (addedElement != null)
            {
                sum.Add(addedElement);
              
                addedElement = addedElement.Next;
            }
        }
        /// <summary>
        /// Сумма двух полиномов.
        /// </summary>
        /// <param name="p1">Первый полином.</param>
        /// <param name="p2">Второй полином.</param>
        /// <returns></returns>
        public static Polinom operator +(Polinom p1, Polinom p2)
        {
            var sum = new Polinom();
            // Указатели на текущие элементы для двух полиномов
            var first = p1.head;
            var second = p2.head;

            for (int i = 0; i < p1.Count + p2.Count; i++)
            {
                // Если один полином обошли, то добавляем остатки второго.
                if (first == null)
                {
                    AddMod(second, sum);
                    break;
                }
                else if (second == null)
                {
                    AddMod(first, sum);
                    break;
                }

                // Если показатели степеней сходятся, то все ок...
                else if (first.VectorOfDegrees.X == second.VectorOfDegrees.X && first.VectorOfDegrees.Y == second.VectorOfDegrees.Y && first.VectorOfDegrees.Z == second.VectorOfDegrees.Z)
                {
                    // Добавляем в результирующий полином, сложив коэффициенты текущих элементов p1 и p2 и меняем указатели.
                    var currentVector = new Vector() { Z = first.VectorOfDegrees.Z, Y = first.VectorOfDegrees.Y, X = first.VectorOfDegrees.X };
                    var current = new Element() { K = first.K + second.K, VectorOfDegrees = currentVector };
                    sum.Add(current);                   
                    first = first.Next;
                    second = second.Next;
                }

                // Иначе добавляем элементы в порядке возрастания степеней...
                else
                {
                    // И после добавления указатель меняем только у элемента с меньшим вектором.
                    if (first.VectorOfDegrees.Z > second.VectorOfDegrees.Z || first.VectorOfDegrees.Y > second.VectorOfDegrees.Y || first.VectorOfDegrees.X > second.VectorOfDegrees.X)
                    {
                        Add(second, sum);
                        second = second.Next;
                    }
                    else
                    {
                        Add(first, sum);
                        first = first.Next;
                    }
                }
            }
            return sum;
        }

        /// <summary>
        /// Добавляет указанный элемент в полином sum.
        /// </summary>
        /// <param name="element">Добавляемый элемент.</param>
        /// <param name="sum">Полином, в который добавляют элемент.</param>
        private static void Add(Element element, Polinom sum)
        {
            var currentVector = new Vector() { Z = element.VectorOfDegrees.Z, Y = element.VectorOfDegrees.Y, X = element.VectorOfDegrees.X };
            var current = new Element() { K = element.K, VectorOfDegrees = currentVector };
            sum.Add(current);
        }
        /// <summary>
        /// Находит первый вектор степеней слагаемого многочлена, имеющий минимальный коэффициент.
        /// </summary>
        /// <returns></returns>
        public Vector Min()
        {
            var min = head.K;
            var minElement = head;
            var currentElement = head;
            for (int i = 0; i < Count; i++)
            {
                if (currentElement.K < min)
                {
                    min = currentElement.K;
                    minElement = currentElement;
                }
                currentElement = currentElement.Next;
            }
            return new Vector() { X = minElement.VectorOfDegrees.X, Y = minElement.VectorOfDegrees.Y, Z = minElement.VectorOfDegrees.Z };
        }

        /// <summary>
        /// Возвращает значение многочлена в некоторой точке.
        /// </summary>
        /// <param name="z"></param>
        /// <param name="y"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public double GetResult(double z, double y, double x)
        {
            var currentElement = head;
            var result = 0.0;

            for (int i = 0; i < Count; i++)
            {
                result += currentElement.K
                         * Math.Pow(x, currentElement.VectorOfDegrees.X)
                         * Math.Pow(y, currentElement.VectorOfDegrees.Y)
                         * Math.Pow(z, currentElement.VectorOfDegrees.Z);
                currentElement = currentElement.Next;
            }
            return result;       
        }

    }
}
