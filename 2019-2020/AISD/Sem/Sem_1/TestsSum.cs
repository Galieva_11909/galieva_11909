﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace Sem_1
{
    [TestFixture]
    public class TestsSum
    {
        [Test]
        public void SumPolinomWithTheSameDegrees()
        {
            var polinom1 = new Polinom();
            polinom1.Build("BuildAPolinom1.txt");
            var polinom2 = new Polinom();
            polinom2.Build("Sum1.txt");
            var expected = new Polinom();
            expected.Build("Sum2.txt");
            Assert.AreEqual(expected, polinom1 + polinom2);
        }

        [Test]
        public void SumPolinomWithDifferenceDegrees()
        {
            var polinom1 = new Polinom();
            polinom1.Build("BuildAPolinom1.txt");
            var polinom2 = new Polinom();
            polinom2.Build("Sum3.txt");
            var expected = new Polinom();
            expected.Build("Sum4.txt");           
            Assert.AreEqual(expected, polinom1 + polinom2);
        }
        
        [Test]
        public void SumPolinomsWithDifferenceAndTheSameDegrees()
        {
            var polinom1 = new Polinom();
            polinom1.Build("BuildAPolinom1.txt");
            var polinom2 = new Polinom();
            polinom2.Build("Sum5.txt");
            var expected = new Polinom();
            expected.Build("Sum6.txt");
            Assert.AreEqual(expected, polinom1 + polinom2);
        }
    }
}
