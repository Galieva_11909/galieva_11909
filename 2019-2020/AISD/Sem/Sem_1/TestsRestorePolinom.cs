﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Sem_1
{
    [TestFixture]
    public class TestsRestorePolinom
    {
      
        [Test]
        public void RestoreOrdinaryPolinomWith1()
        {
            var polinom = BuildPolinom();
            polinom.RestorePolinom(1, "RestorePolinom1.txt");
            var data = File.ReadAllText("RestorePolinom1.txt");
            var expectedData = "1 2 3 4\n2 4 6 8\n3 6 9 12\n4 8 12 16\n5 10 15 20\n6 12 18 24\n7 14 21 28\n8 16 24 32\n9 18 27 36\n10 20 30 40\n";
            Assert.AreEqual(expectedData, data);
        }

        [Test]
        public void RestoreOrdinaryPolinomWith0()
        {
            var polinom = BuildPolinom();
            polinom.RestorePolinom(0, "RestorePolinom2.txt");
            var data = File.ReadAllText("RestorePolinom2.txt");
            var expectedData = "1*z^2*y^3*x^4+2*z^4*y^6*x^8+3*z^6*y^9*x^12+4*z^8*y^12*x^16+5*z^10*y^15*x^20+6*z^12*y^18*x^24+7*z^14*y^21*x^28+8*z^16*y^24*x^32+9*z^18*y^27*x^36+10*z^20*y^30*x^40";
            Assert.AreEqual(expectedData, data);
        }
       
        [Test]
        public void IsEmptyMemory()
        {
            var polinom = BuildPolinom();
            polinom.RestorePolinom(1, "RestorePolinom2.txt");
            var expected = new Polinom();
            Assert.AreEqual(expected, polinom);
        }
        public Polinom BuildPolinom()
        {
            var polinom = new Polinom();
            for (int i = 1; i < 11; i++)
            {
                var currentVector = new Vector() { Z = i * 2, Y = i * 3, X = i * 4 };
                var currentElement = new Element() { K = i, VectorOfDegrees = currentVector };
                polinom.Add(currentElement);
            }
            return polinom;
        }
    }
}
