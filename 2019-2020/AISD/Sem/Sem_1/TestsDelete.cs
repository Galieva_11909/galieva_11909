﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace Sem_1
{
    [TestFixture]
    public class TestsDelete
    {
        [Test]
        public void DeleteElementInTheMiddle()
        {
            var actualPolinom = new Polinom();
            actualPolinom.Build("BuildAPolinom1.txt");
            actualPolinom.Delete(10, 15, 20);
            var expectedPolinom = new Polinom();
            expectedPolinom.Build("Delete1.txt");
            Assert.AreEqual(expectedPolinom, actualPolinom);            
        }

        [Test]
        public void DeleteElementAtTheBeginning()
        {
            var actualPolinom = new Polinom();
            actualPolinom.Build("BuildAPolinom1.txt");
            actualPolinom.Delete(2, 3, 4);
            var expectedPolinom = new Polinom();
            expectedPolinom.Build("Delete2.txt");
            Assert.AreEqual(expectedPolinom, actualPolinom);
        }

        [Test]
        public void DeleteElementInTheEnd()
        {
            var actualPolinom = new Polinom();
            actualPolinom.Build("BuildAPolinom1.txt");
            actualPolinom.Delete(20, 30, 40);
            var expectedPolinom = new Polinom();
            expectedPolinom.Build("Delete3.txt");
            Assert.AreEqual(expectedPolinom, actualPolinom);
        }

        [Test]
        public void DeleteNonexistentElement()
        {
            var polinom = new Polinom();
            polinom.Build("BuildAPolinom1.txt");
            var actual = Assert.Throws<FormatException>(() => polinom.Delete(17, 18, 19));
            Assert.That(actual.Message, Is.EqualTo("Элемента с такими показателями не существует"));
        }


    }
}
