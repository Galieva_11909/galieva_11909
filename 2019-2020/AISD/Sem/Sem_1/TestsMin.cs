﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace Sem_1
{
    [TestFixture]
    public class TestsMin
    {
        [Test]
        public void MinVector()
        {
            var polinom = new Polinom();
            polinom.Build("Min1.txt");
            Assert.AreEqual(new Vector() { Z = 14, Y = 21, X = 28 }, polinom.Min());
        }

        [Test]
        public void FindFirstMinVector()
        {
            var polinom = new Polinom();
            polinom.Build("Min2.txt");
            Assert.AreEqual(new Vector() { Z = 8, Y = 12, X = 16 }, polinom.Min());
        }

        [Test]
        public void FindMinInHead()
        {
            var polinom = new Polinom();
            polinom.Build("Min3.txt");
            Assert.AreEqual(new Vector() { Z = 2, Y = 3, X = 4 }, polinom.Min());
        }

        [Test]
        public void FindMinInTail()
        {
            var polinom = new Polinom();
            polinom.Build("Min4.txt");
            Assert.AreEqual(new Vector() { Z = 20, Y = 30, X = 40 }, polinom.Min());
        }
    }
}
