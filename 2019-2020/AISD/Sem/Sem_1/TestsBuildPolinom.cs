﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace Sem_1
{
    [TestFixture]    
    public class TestsBuildPolinom
    {
        
        [Test]
        public void BuildAPolinom()
        {
            var expectedPolinom = new Polinom();
            for (int i = 1; i < 11; i++)
            {
                var currentVector = new Vector() { Z = i * 2, Y = i * 3, X = i * 4 };
                var currentElement = new Element() { K = i, VectorOfDegrees = currentVector };
                expectedPolinom.Add(currentElement);
            }
            var actualPolinom = new Polinom();
            actualPolinom.Build("BuildAPolinom1.txt");
            Assert.That(expectedPolinom, Is.EqualTo(actualPolinom));
           
        }

        [Test]
        public void EmptyFile()
        {
            var polinom = new Polinom();
            var actual = Assert.Throws<FormatException>(() => polinom.Build("BuildAPolinom2.txt"));
            Assert.That(actual.Message, Is.EqualTo("Пустой файл"));            
        }
    }
}
