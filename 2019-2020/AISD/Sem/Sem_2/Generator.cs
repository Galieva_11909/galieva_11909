﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semestrovka
{
    class Generator
    {
        public static void Generate(int max)
        {
            // 75 наборов
            var rnd = new Random();
            for (int i = 0; i < 4; i++)
            {
                // Кол-во элементов в наборе
                var elements = rnd.Next(100, max);
                var fileName = $"{i}.txt";
                var numbers = new StringBuilder();
                for (int j = 0; j < elements; j++)
                {
                    var current = rnd.Next();
                    numbers.Append(current + "\r");
                }
                File.WriteAllText(fileName, numbers.ToString());
            }

        }
    }
}