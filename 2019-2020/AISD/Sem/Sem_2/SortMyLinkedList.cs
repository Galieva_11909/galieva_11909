﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Semestrovka
{
    class SortMyLinkedList : ITask
    {
        private MyLinkedList data;
        public static int Iterations;
        public SortMyLinkedList(MyLinkedList list)
        {
            data = list;
            Iterations = 0;
        }
        // Function to perform Introsort on unsorted array elements.
        public static void Introsort(ref MyLinkedList array, int start, int end, int maxdepth)
        {
            // calculating length of array through length method while passing refernece of array , start index and end index
            int leng = length(ref array, start, end);

            if (leng <= 1)
            {
                // base case
                // array is sorted so return the sorted array
                return;
            }

            else if (maxdepth == 0)
            {
                //use heapsort if maximumdepth equals to zero by passing reference of unsorted array.
                heapsort(ref array);
            }

            else
            {
                //perform Quick sort
                int partitionpos = partition(array, start, array.Count - 1);
                Introsort(ref array, 0, partitionpos - 1, maxdepth - 1);
                Introsort(ref array, partitionpos + 1, leng, maxdepth - 1);
            }

        }

        static int length(ref MyLinkedList array2, int beginning, int ending)
        {
            int len = 0;

            if (beginning <= ending)
            {
                for (int i = beginning; i <= ending; i++)
                {
                    len++;
                    Iterations++;
                }

            }
            return len;
        }

        //  A function to partition the given array and to return the partition position
        static int partition(MyLinkedList a, int start, int end)
        {
            int tem;
            int left = start; int right = end;
            int pivot = a[start];  // Pivot

            // find pivot position until left index is less than right index.
            while (left < right)
            {
                // until current left index value is less than pivot value increment left index by 1. 
                while (a[left] < pivot)
                {
                    left++;
                    Iterations++;
                }

                // until current right index value is greater than pivot value decrement right index by 1.
                while (a[right] > pivot)
                {
                    right--;
                    Iterations++;
                }

                if (left < right)
                {
                    //swap left index value and right index value
                    tem = a[left];
                    a[left] = a[right];
                    a[right] = tem;

                    //if left index value and right index value is same then increment left index by 1.
                    if (a[left] == a[right])
                        left++;
                }
                else
                {
                    return right;
                }

            }

            //returning correct position of pivot
            return right;
        }

        // Implementing Heapsort
        static void heapsort(ref MyLinkedList array1)
        {
            int heapsize = array1.Count; // array length is the heapsize for creating heap

            // Build heap ( Rearrange array )
            for (int m = (heapsize / 2) - 1; m >= 0; m--)
            {
                heapify(ref array1, heapsize, m);
                Iterations++;
            }

            // remove an element from heap
            // one by one
            for (int n = array1.Count - 1; n >= 0; n--)
            {
                int temp = array1[n];
                array1[n] = array1[0];
                array1[0] = temp;
                Iterations++;
                --heapsize;
                // creates maximum heap on the reduced heap by calling heapify function.
                heapify(ref array1, heapsize, 0);
            }

        }

        // creates heap of subtree
        static void heapify(ref MyLinkedList array3, int x, int y)
        {
            int max = 0;
            int r = (y + 1) * 2; // right child 
            int l = ((y + 1) * 2) - 1; // left child

            if (l < x && array3[l] > array3[y])
            {
                max = l;
            }
            else
            {
                max = y;
            }
            if (r < x && array3[r] > array3[max])
            {
                max = r;
            }
            if (max != y)
            {
                int temp;
                temp = array3[y];
                array3[y] = array3[max];
                array3[max] = temp;

                // recursively heapify the affected sub-tree
                heapify(ref array3, x, max);
            }
        }

        public void Run()
        {
            Introsort(ref this.data, 0, this.data.Count - 1, Convert.ToInt16(2 * Math.Log(this.data.Count)));
        }
    }
}
