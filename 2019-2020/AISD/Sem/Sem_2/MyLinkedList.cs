﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semestrovka
{
    public class Element
    { 
        public int Index;
        public int Value;
        public Element Next;
    }

    public class MyLinkedList
    {
        private Element head;
        private Element tail;
        public int Iterations { get; private set; }
        /// <summary>
        /// Количество векторов показателей степеней (одночленов) в данном полиноме.
        /// </summary>
        public int Count { get; private set; }

        public void Add(Element current)
        {
            if (head == null)
            {
                head = current;
                tail = current;
            }
            else
            {
                tail.Next = current;
                tail = current;
            }
            this.Count++;
        }

        public MyLinkedList(int[] array)
        {
            for (var i = 0; i < array.Length; i++)
            {
                var current = new Element() {Value = array[i], Index = i};
                this.Add(current);
            }
        }

        public int this[int index]
        {
            get
            {
                var current = head;
                while (current.Index != index)
                {
                    Iterations++;
                    current = current.Next;
                }
                return current.Value;
            }
            set
            {
                var current = head;
                while (current.Index != index)
                {
                    Iterations++;
                    current = current.Next;
                }
                current.Value = value;
            }
        }
    }
}
