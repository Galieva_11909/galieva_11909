﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Semestrovka
{
    class Program
    {
        static void Main(string[] args)
        {
            Generator.Generate(1000);
            Run(1);
            Run(2);
        }
        public static void Run(int z)
        {
            if (z == 1)
                RunArray();
            else
            {
                RunList();
            }
        }

        private static void RunList()
        {
            var linkedListTime = new StringBuilder();
            for (int i = 0; i < 20; i++)
            {
                var fileName = $"{i}.txt";
                var array = File.ReadAllText(fileName).Split('\r').Where(m => m != "").Select(m => int.Parse(m)).ToArray();
                var linkedList = new MyLinkedList(array);
                var linkedListAction = new SortMyLinkedList(linkedList);
                linkedListTime.Append(linkedList.Count + "\t" + MeasureDurationInMs(linkedListAction) + "\t" +
                                      (SortMyLinkedList.Iterations + linkedList.Iterations) + "\r");
            }
            File.WriteAllText("LinkedListResult.txt", linkedListTime.ToString());
        }
        private static void RunArray()
        {
            var arrayTime = new StringBuilder();
            for (int i = 0; i < 4; i++)
            {
                var fileName = $"{i}.txt";
                var array = File.ReadAllText(fileName).Split('\r').Where(m => m != "").Select(m => int.Parse(m)).ToArray();
                var arrayAction = new SortArray(array);
                arrayTime.Append(array.Length + "\t" + MeasureDurationInMs(arrayAction) + "\t" + SortArray.Iterations + "\r");
            }
            File.WriteAllText("ArrayResult.txt", arrayTime.ToString());
        }

        public static double MeasureDurationInMs(ITask task)
        {
            GC.Collect();                   // Эти две строчки нужны, чтобы уменьшить вероятность того,
            GC.WaitForPendingFinalizers();  // что Garbadge Collector вызовется в середине измерений
                                            // и как-то повлияет на них.
            Stopwatch time = new Stopwatch();
            time.Start();
            task.Run();
            for (int i = 0; i < 3; i++)
            {
                task.Run();
            }
            time.Stop();
            var milliSec = time.ElapsedMilliseconds;
            return (double)milliSec / 3;
        }

        private static LinkedList<int> CreateLinkedList(int[] array)
        {
            var result = new LinkedList<int>();
            for (var i = 0; i < array.Length; i++)
            {
                result.AddLast(array[i]);
            }
            return result;
        }

    }
}
