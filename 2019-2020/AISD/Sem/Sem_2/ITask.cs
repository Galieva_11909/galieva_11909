﻿namespace Semestrovka
{
    public interface ITask
    {
        void Run();
    }
}