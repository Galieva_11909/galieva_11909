﻿using System;

namespace Fibonacci_number_recursion
{
    class Fib_recursion
    {
         /* 
          * F(n) = F(n-1) + F(n-2) > 2*F(n-2), поэтому F(n) > 2^(0.5n)
          * Из дерева рекурсаа ясно, что (n-k)-ый элемент вычисляется повторно k раз => Неэффективный по времени алгоритм.
          * 
        */
        public int Recursion(int n)
        {
            if (n == 1 || n == 0)
                return n;
            return Recursion(n - 1) + Recursion(n - 2);
        }
       
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
