﻿using System;
using System.Linq;
using System.Text;

namespace FindUnion
{
    public class Connection
    {
        int[] Connects;
        // Для корня каждого дерева указана высота этого дерева.
        int[] Hegths;        
        public Connection(int alph)
        {
            Connects = new int[alph + 1];
            Hegths = new int[alph + 1];
            for (int i = 0; i < alph+1; i++)
            {
                Connects[i] = i;
                Hegths[i] = 1;
            }
        }

        private int ToGoByRoad(int a, int b, StringBuilder road, int title)
        {
            for (int i = a; ;)
            {
                road.Append(i);
                // Если числа находятся в одной компоненте связности...
                // Т.е. если число a указывает на b (в т.ч. через другие числа, то объединять ничего не нужно).
                if (Connects[i] == b)
                {
                    road.Append(b);
                    break;
                }
                // Если мы дошли не до корня дерева...
                if (Connects[i] != i)
                {
                    i = Connects[i];
                }
                else
                {
                    title = i;
                    break;
                }
                continue;
            }
            return title;
        }
        string Reverse(StringBuilder road)
        {
            var temporary = new StringBuilder();
            for (int i = road.Length - 1; i >= 0; i--)
            {
                temporary.Append(road[i]);
            }
            return temporary.ToString();
        }
        void Find(int a, int b)
        {
            // Путь до корня дерева.
            var road = new StringBuilder();

            // Корени деревьев для чисел а и b.
            var titleA = -1;
            var titleB = -1;

            // Ищем путь до корня сначала одного, потом другого числа
            titleA = ToGoByRoad(a, b, road, titleA);
            var roadA = road.ToString();          
            road.Clear();
            titleB = ToGoByRoad(b, a, road, titleB);
            if (titleB == titleA)
                road.Remove(road.Length - 1, 1);
            // Путь из корня в элемент b
            var roadB = Reverse(road);
            
            Union(titleA, titleB, roadA, roadB);
        }

        void Union(int rootA, int rootB, string Road, string RoadB)
        {
            // Если высота первого дерева больше высоты второго, то меняем указатель у второго
            if (Hegths[rootA] > Hegths[rootB])
            {
                Connects[rootB] = rootA;
                // Если при присоединении высота дерева увеличится, то указываем это в массиве высот.
                if (Hegths[rootB] + 1 > Hegths[rootA])
                {
                    Hegths[rootA] = Hegths[rootB] + 1;
                }
            }
            else
            {
                Connects[rootA] = rootB;
                if (Hegths[rootA] + 1 > Hegths[rootB])
                {
                    Hegths[rootB] = Hegths[rootA] + 1;
                }
            }
            Console.WriteLine(Road+RoadB);
        }

        public void ToGetRoadFromAtoB(int a, int b)
        {
            Find(a, b);
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите N: ");
            var n = int.Parse(Console.ReadLine());
            var connects = new Connection(n);
            string road = "-1";
            while (road != null)
            {
                Console.WriteLine("Введите через тире числа для вывода пути между ними: ");
                var numbers = Console.ReadLine().Split('-');
                var a = int.Parse(numbers[0]);
                var b = int.Parse(numbers[1]);
                connects.ToGetRoadFromAtoB(a, b);
            }
        }
    }
}
