﻿using System;

namespace iterations
{
    class Fib
    {
        // Сложность линейная (в точности 4*(n-1)
        public int Fib_iterations(int n)
        {
            var c = 0;
            var a = 1;
            var b = 1;
            if (n == 0 || n == 1)
                return n;
            for (var i =3; i<=n; i++)
            {
                c = a + b;
                a = b;
                b = c;
            }
            return c;
        }

        static void Main(string[] args)
        {
        }
    }
}
