﻿using System;
using System.Collections.Generic;

namespace Pocket
{

    public class Program
    {
        // Возвращает длину наибольшего слова
        public static int ToGetMaxLenth(string[] words)
        {
            var max = 0;
            // Находим наибольшую длину строки 
            foreach (var word in words)
            {
                if (word.Length >= max)
                {
                    max = word.Length;
                }
            }
            return max;
        }
        public static void ToDoOneLength(string[] words, int n, int max, string[] alph)
        {
            // !!! Как вернуть строки в изначальное состояние?
            for (int i = 0; i < n; i++)
            {
                if (words[i].Length != max)
                {
                    var res = max - words[i].Length;
                    words[i] = new String(alph[0][0], res) + words[i];
                }
            }
        }

        public static string[] ToFillPockets(int max, string[] words, Dictionary<char, Queue<string>> pockets, string[] alph, int n, int m)
        {
            for (int i = max - 1; i >= 0; i--)
            {
                //Каждого слова...
                for (int j = 0; j < n; j++)
                {
                    for (int k = 0; k < m; k++)
                    {
                        // Если символ слова равен символу алфавита, то добавляем его в карман.
                        if (words[j][i] == alph[k][0]) // words[j]
                        {
                            pockets[alph[k][0]].Enqueue(words[j]);
                            break;
                        }// переход.    Другой символ АЛФАВИТА.
                    } // переход.       Следующее слово.
                } // переход.            Распределение по следующему символу.

                var counter = 0; // counter <= n
                for (int k = 0; k < m; k++)
                {
                    while (pockets[alph[k][0]].Count != 0)
                    {
                        words[counter] = pockets[alph[k][0]].Dequeue();
                        counter++;
                    }
                } // переход.           Следующий символ СЛОВА
            }
            var array = words;
            return array;
        }

        static void Main(string[] args)
        {

            // Набор строк
            Console.WriteLine("Введите набор символов через пробел:");
            var words = Console.ReadLine().Split(' ');
            var n = words.Length;
            if (words[0] != "")
            {
                var max = ToGetMaxLenth(words);

                // Алфавит 
                Console.WriteLine("Введите символы алфавита через пробел:");
                var alph = Console.ReadLine().Split(' ');
                var m = alph.Length; // ДЛИНА!!!!

                // Карманы.            
                var pockets = new Dictionary<char, Queue<string>>();
                for (int i = 0; i < m; i++)
                {
                    pockets[alph[i][0]] = new Queue<string>();
                }

                // Если строки в наборе имеют разную длину, то добавляем спереди самый первый символ алфавита 
                ToDoOneLength(words, n, max, alph);

                // Заполняем карманы
                words = ToFillPockets(max, words, pockets, alph, n, m);
                foreach (var word in words)
                    Console.WriteLine(word);
            }
            else
                Console.WriteLine("Пустая строка");
        }
    }

}