﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;


namespace Check_system
{
    ////////////////////////////////////////////////////////// См. на пути до папок (будет работать некорректно, если не изменить) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    public class Student
    {
        public string Name;
        public List<string> Exersises;

        public Student(string name, List<string> exersises)
        {
            Name = name;
            Exersises = exersises;
        }
    }
    class Program
    {
        public static void CheckWork(List<string> expectedEx, string studentName,
            Dictionary<string, (string, Type[])> allExersises, Dictionary<string, List<(object, object[])>> allTests)
        {
            var pathToWorks =
                $@"C:\Users\Railina\source\repos\Check system\bin\Debug\Check system\Students\{studentName}"; // Путь до папки студента, в которой лежат dll.
            // Выполненные задания
            var factExersise = Directory.GetFiles(pathToWorks).Select(m => Path.GetFileName(m))
                .Select(m => m.Substring(0, m.Length - 4)).ToList();
            CheckIsAllClassesRight(studentName, allExersises, factExersise, allTests);
            CheckIsAllWorksWasDone(factExersise, expectedEx, studentName);
        }

        static void Main(string[] args)
        {
            var pathToNames =
                @"C:\Users\Railina\source\repos\Check system\bin\Debug\Check system\Variants"; // Путь до папки Variants
            // Получаем список всех студентов и записываем их в массив.
            var listOfNames = Directory.GetFiles(pathToNames).Select(m => Path.GetFileName(m))
                .Select(m => m.Substring(0, m.Length - 4)).ToArray();

            // Получаем список всех упражнений и также записываем их в массив
            var pathExersises =
                @"C:\Users\Railina\source\repos\Check system\bin\Debug\Check system\Exercises"; // Путь до папки Exersises
            var listOfExersisesName = Directory.GetFiles(pathExersises).Select(m => Path.GetFileName(m))
                .Select(m => m.Substring(0, m.Length - 4)).ToArray();
            
            var allExersises = CreateDictionaryWithExersises(listOfExersisesName);
            var allTests = CreateTests(listOfExersisesName, allExersises);

            // Список студентов с их упражненями, которые должны быть ими выполнены.
            var expextedStudentsWork = new Student[listOfNames.Length];
            // Заполяем список.
            for (var i = 0; i < listOfNames.Length; i++)
            {
                // Путь до файла, в котором лежит список вариантов, которые нужно выполнить студенту
                var pathToEx =
                    $@"C:\Users\Railina\source\repos\Check system\bin\Debug\Check system\Variants\{listOfNames[i]}.txt"; // Н-р. путь до файла Ivanov.txt, лежащий в Variants
                expextedStudentsWork[i] = new Student(listOfNames[i], File.ReadAllLines(pathToEx).ToList());
            }

            //TODO: разбить по потокам!!!
            foreach (var student in expextedStudentsWork)
            {
                Console.WriteLine(student.Name);
                CheckWork(student.Exersises, student.Name, allExersises, allTests);
            }

            Console.ReadKey();
        }

        // Первый словарь: exersise_1 - (classAndMethodName, ArgumentsType[] (in alphabit order))
        /// <summary>
        ///     Создает словарь, в которм ключ - название упражнения, значение - пара (1, 2), где
        ///     1 - название класса и название метода
        ///     2 - список типов аргументов метода.
        /// </summary>
        /// <param name="listOfExersisesName">Список с названиями упражнений</param>
        /// <returns></returns>
        private static Dictionary<string, (string, Type[])> CreateDictionaryWithExersises(string[] listOfExersisesName)
        {
            var result = new Dictionary<string, (string, Type[])>();
            var pathToEx = @"C:\Users\Railina\source\repos\Check system\bin\Debug\Check system\Exercises";
            foreach (var exersise in listOfExersisesName)
            {
                //TODO: разобраться с путем в катологах
                var path = $@"{pathToEx}\{exersise}.txt";
                var data = File.ReadAllLines(path)
                    .Skip(1)
                    .Select(m => m.Split(':'))
                    .ToArray();
                var argumentsType = data.Skip(2)
                    .OrderBy(m => m[0])
                    .ToList();
                var parametrs = new List<Type>();
                for (var i = 0; i < data.Length - 1; i++)
                {
                    var tempoary = i == 0 ? data[1][0] : argumentsType[i - 1][1];
                    switch (tempoary)
                    {
                        case "int":
                            parametrs.Add(typeof(int));
                            break;
                        case "double":
                            parametrs.Add(typeof(double));
                            break;
                        case "string":
                            parametrs.Add(typeof(string));
                            break;
                        case "char":
                            parametrs.Add(typeof(char));
                            break;
                        case "bool":
                            parametrs.Add(typeof(bool));
                            break;
                        default:
                            throw new Exception("Неверный формат типа перменной");
                    }
                }

                result[exersise] = (data[0][0], parametrs.ToArray());
            }

            return result;
        }

        /// <summary>
        ///     Проверяет, все ли номера выполнены студентом.
        /// </summary>
        /// <param name="factExersise"></param>
        /// <param name="expectedEx"></param>
        private static void CheckIsAllWorksWasDone(List<string> factExersise, List<string> expectedEx,
            string studentName)
        {
            // Скорее всего убрать в отдельный метод
            foreach (var exersise in expectedEx)
            {
                if (!factExersise.Contains(exersise))
                {
                    Console.WriteLine($"Вариант {exersise}: не выполнен");
                    File.AppendAllLines(
                        $@"C:\Users\Railina\source\repos\Check system\bin\Debug\Check system\Results\{studentName}.txt",
                        new[] {$"Вариант {exersise}: не выполнен"});
                }
            }
        }

        /// <summary>
        ///     Проверяет, правильно ли выполнены задания.
        /// </summary>
        /// <param name="allExersises"></param>
        /// <param name="factExersises"></param>
        /// <param name="allTests"></param>
        private static void CheckIsAllClassesRight(string studentName,
            Dictionary<string, (string, Type[])> allExersises, List<string> factExersises,
            Dictionary<string, List<(object, object[])>> allTests)
        {
            foreach (var nameOfFactExersise in factExersises)
            {
                var nameOfNeedClassAndMethod = allExersises[nameOfFactExersise].Item1.Split('.');
                var pathToDll =
                    $@"C:\Users\Railina\source\repos\Check system\bin\Debug\Check system\Students\{studentName}\{nameOfFactExersise}.dll";

                //  Загружаем dll-файл
                var assembly = Assembly.LoadFile(pathToDll);
                // Получаем тип, оперделенный в сборке
                var classFromDll = assembly.GetTypes()[0];
                if (classFromDll.Name == nameOfNeedClassAndMethod[0])
                {
                    var obj = Activator.CreateInstance(classFromDll);
                    var method = classFromDll.GetMethods()[0];
                    if (method.Name != nameOfNeedClassAndMethod[1])
                    {
                        Console.WriteLine($"Вариант {nameOfFactExersise}: нет подходящего метода");
                        File.AppendAllLines(
                            $@"C:\Users\Railina\source\repos\Check system\bin\Debug\Check system\Results\{studentName}.txt",
                            new[] {$"Вариант {nameOfFactExersise}: нет подходящего метода"});
                    }
                    else
                    {
                        // Если метод с такими аргументами существует, тесируем его.
                        RunTest(method, nameOfFactExersise, obj, allTests, studentName);
                    }
                }
                else
                {
                    Console.WriteLine($"Вариант {nameOfFactExersise}: нет подходящего класса");
                    File.AppendAllLines(
                        $@"C:\Users\Railina\source\repos\Check system\bin\Debug\Check system\Results\{studentName}.txt",
                        new[] {$"Вариант {nameOfFactExersise}: нет подходящего класса"});
                }
            }
        }

        private static void RunTest(MethodInfo method, string nameOfEx, object obj,
            Dictionary<string, List<(object, object[])>> testsDictionary, string studentName)
        {
            var currentTests = testsDictionary[nameOfEx];
            var isRight = true;
            foreach (var currentTest in currentTests)
            {
                var result = method.Invoke(obj, currentTest.Item2);
                if (!result.Equals(currentTest.Item1))
                {
                    isRight = false;
                    break;
                }
            }

            if (isRight)
            {
                Console.WriteLine($"Вариант {nameOfEx}: работает корректно");
            }
            else
            {
                Console.WriteLine($"Вариант {nameOfEx}: неверное значение при выполнении теста");
                File.AppendAllLines(
                    $@"C:\Users\Railina\source\repos\Check system\bin\Debug\Check system\Results\{studentName}.txt",
                    new[] {$"Вариант {nameOfEx}: нет подходящего класса"});
            }
        }

        /// <summary>
        ///     Создает словарь, в котором ключ - название упражнения, а значение список пар (1,2) , где
        ///     1 - значение, которое должно быть выведено
        ///     2 - аргументы
        /// </summary>
        /// <param name="listOfExersises"></param>
        /// <param name="allExersises"></param>
        /// <returns></returns>
        static Dictionary<string, List<(object, object[])>> CreateTests(string[] listOfExersises,
            Dictionary<string, (string, Type[])> allExersises)
        {
            var result = new Dictionary<string, List<(object, object[])>>();
            foreach (var exersise in listOfExersises)
            {
                var pathToTests =
                    $@"C:\Users\Railina\source\repos\Check system\bin\Debug\Check system\Tests\{exersise}";
                var listNameOfTests = Directory.GetFiles(pathToTests).Select(m => Path.GetFileName(m))
                    .Select(m => m.Substring(0, m.Length - 4)).ToList();
                var listOfTests = new List<(object, object[])>();
                var types = allExersises[exersise].Item2;
                foreach (var test in listNameOfTests)
                {
                    var data = File.ReadAllLines($@"{pathToTests}\{test}.txt").Select(m => m.Split(':')).ToArray();
                    var resultValue = data[data.Length - 1][0];
                    data = data.Take(data.Length - 1).OrderBy(m => m[0]).ToArray();
                    var temporaryValuesList = new List<object>();
                    var rightResult = default(object);
                    for (var i = 0; i < data.Length + 1; i++)
                    {
                        var valueOfVariable = i == 0 ? resultValue : data[i - 1][1];
                        //TODO: разобраться с выходным значением
                        var temporary = CheckType(valueOfVariable, types[i]);
                        if (i == 0)
                            rightResult = temporary;
                        else
                            temporaryValuesList.Add(temporary);
                    }

                    listOfTests.Add((rightResult, temporaryValuesList.ToArray()));
                }

                result[exersise] = listOfTests;
            }

            return result;
        }

        private static object CheckType(string valueOfVariable, Type type)
        {
            object temporary;
            if (type == typeof(int))
                temporary = int.Parse(valueOfVariable);
            else if (type == typeof(double))
                temporary = double.Parse(valueOfVariable);
            else if (type == typeof(string))
                temporary = valueOfVariable;
            else if (type == typeof(char))
                temporary = char.Parse(valueOfVariable);
            else if (type == typeof(bool))
                temporary = bool.Parse(valueOfVariable);
            else
                throw new Exception("Неверный формат типа перменной");
            return temporary;
        }
    }
}