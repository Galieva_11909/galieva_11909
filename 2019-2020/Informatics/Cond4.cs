﻿using System;

namespace ConsoleApp1
{
    class Cond4
    {

        static void Main(string[] args)
        {
            //Cond4.Пересечение двух отрезков[A, B] и[C, D] на числовой прямой.

            Console.WriteLine("Введите a, b, c, d через пробел: ");
            var k = Console.ReadLine();
            var values = k.Split(" ");
            var a = double.Parse(values[0]);
            var b = double.Parse(values[1]);
            var c = double.Parse(values[2]);
            var d = double.Parse(values[3]);

            var length = -1.0;

            if (a < c && b < c || c < a && d < a)
                Console.WriteLine("Отрезки не пересекаются");
            else
                length = Math.Abs(Math.Max(a, c) - Math.Min(b, d));
            if (length == 0)
                Console.WriteLine("Пересекаются в точке {0}", Math.Max(a, c));
            else
                Console.WriteLine("Пересекаются, длина отрезка: {0}", length);

        }

    }
}

