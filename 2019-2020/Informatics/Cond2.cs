﻿using System;

namespace ConsoleApp1
{
    class Cond2
    {

        static void Main(string[] args)
        {
            //Пролезет ли брус со сторонами x, y, z в отверстие со сторонами a, b, если его разрешается поворачивать на 90 градусов?

            Console.WriteLine("Введите x, y, z, a, b через пробел: ");
            var k = Console.ReadLine();
            var values = k.Split(" ");
            var x = double.Parse(values[0]);
            var y = double.Parse(values[1]);
            var z = double.Parse(values[2]);
            var a = double.Parse(values[3]);
            var b = double.Parse(values[4]);

            var square = a * b;
            if (square == x * y || square == x * z || square == y * z)
                Console.WriteLine("Брусок пролезет");
            else
                Console.WriteLine("Брусок не пролезет");

        }

    }
}

