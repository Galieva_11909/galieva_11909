﻿using System;

namespace ConsoleApp1
{
    class Cond4
    {

        static void Main(string[] args)
        {

            Console.WriteLine("Введите l, k, h через пробел: ");
            var z = Console.ReadLine();
            var values = z.Split(" ");
            var l = double.Parse(values[0]);
            var k = double.Parse(values[1]);
            var h = double.Parse(values[2]);
            var max = 0.0;

            if (l % k == 0)
                max = l / k + 1;
            else
                max = l / k;

            Console.WriteLine("{0}, {1}", (double)(l / k * h), (double)max * h);

        }

    }
}

