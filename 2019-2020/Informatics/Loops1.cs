﻿using System;

namespace ConsoleApp1
{
    class Loops1
    {

        static void Main(string[] args)
        {

            Console.WriteLine("Введите число: ");
            var n = Console.ReadLine();
            var tenPower = Math.Pow(10, n.Length - 1);
            var N = int.Parse(n);
            var result = 0.0;
            
            while (N > 0)
            {
                var digit = N % 10;
                N /= 10;
                result += digit * tenPower;
                tenPower /= 10;
            }
            Console.WriteLine("{0}", result);

        }

    }
}

