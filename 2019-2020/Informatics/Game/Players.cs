﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame
{
    public class Player : IPlayer
    {
        public string NameOfImage()
        {
            return "Player.png";
        }

        // Может двигаться только вправо и влево
        public Commands Act(int x, int y)
        {
            var moves = new Commands();
            var key = Game.KeyPressed;
            if (key == System.Windows.Forms.Keys.Right && x + 1 < Game.MapWidth)
            {
                moves.DeltaX = 1;
            }

            if (key == System.Windows.Forms.Keys.Left && x - 1 >= 0)
            {
                moves.DeltaX = -1;
            }

            return moves;
        }

        public bool DeadInConflict(IPlayer conflictedObject)
        {
            return false; // Никогда не умирает
        }

        public class Bullet : IPlayer
        {
            public string NameOfImage()
            {
                return "Bullet.png";
            }
            
            public bool DeadInConflict(IPlayer conflictedObject)
            {
                return conflictedObject is Enemy; // Исчезает, если оказывается в одной клетке с игроком
            }

            public Commands Act(int x, int y)
            {

                var moves = new Commands();// Движется прямо по курсу, когда запускается 
                moves.DeltaY--;
                return moves;

            }
        }
        public class Enemy : IPlayer
        {
            public Commands Act(int x, int y)
            {
                var moves = new Commands(); // Движется вниз
                moves.DeltaY++;
                return moves;
            }

            public bool DeadInConflict(IPlayer conflictedObject)
            {
                return conflictedObject is Bullet || conflictedObject is Player;
            }

            public string NameOfImage()
            {
                return "Enemy.png";
            }
        }
    }
}
