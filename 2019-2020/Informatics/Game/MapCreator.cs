﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MyGame
{
    public static class MapCreator
    {
        private static readonly Dictionary<string, Func<IPlayer>> factory = new Dictionary<string, Func<IPlayer>>();

        private static IPlayer Create(char c)
        {
            switch (c)
            {
                case 'P':
                    return CreatePlayer("Player");
                case 'E':
                    return CreatePlayer("Enemy");
                case 'B':
                    return CreatePlayer("Bullet");
                case ' ':
                    return null;
                default:
                    throw new Exception($"wrong character for ICreature {c}");
            }
        }

        private static IPlayer CreatePlayer(string name)
        {
            if (!factory.ContainsKey(name))
            {
                var type = Assembly
                    .GetExecutingAssembly()
                    .GetTypes()
                    .FirstOrDefault(z => z.Name == name);
                if (type == null)
                    throw new Exception($"Can't find type '{name}'");
                factory[name] = () => (IPlayer)Activator.CreateInstance(type);
            }

            return factory[name]();
        }

    }
}