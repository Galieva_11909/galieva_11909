﻿namespace MyGame
{
    public interface IPlayer
    {
        string NameOfImage();
        Commands Act(int x, int y);
        bool DeadInConflict(IPlayer conflictedObject);
    }
}