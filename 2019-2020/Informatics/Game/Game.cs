﻿using System.Windows.Forms;

namespace MyGame
{
    // ВНизу игрок, сверху движутся враги..
    public static class Game
    {
        
        private const string map = @"


   P ";

        public static IPlayer[,] Map;
        public static int Scores;
        public static bool IsOver;

        public static Keys KeyPressed;
        public static int MapWidth => Map.GetLength(0);
        public static int MapHeight => Map.GetLength(1);

        public static void CreateMap()
        {
            Map = MapCreator.CreateMap(map);
        }
    }
}