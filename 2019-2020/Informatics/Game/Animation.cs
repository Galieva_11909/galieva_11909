﻿using System.Drawing;
using MyGame;

namespace Game
{
    public class Animation
    {
        public Commands Command;
        public IPlayer Player;
        public Point Location;
        public Point TargetLogicalLocation;
    }
}