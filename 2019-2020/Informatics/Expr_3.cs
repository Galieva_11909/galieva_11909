﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Seminarskie
{
    class Expr_3
    {
        static void Main(string[] args)
        {
            //Задано время Н часов (ровно). Вычислить угол в градусах между часовой и минутной стрелками.
            //Например, 5 часов -> 150 градусов, 20 часов -> 120 градусов. Не использовать циклы.

            var n = int.Parse(Console.ReadLine());
           
            if (n > 12)
            {
                n = n - 12;
            }

            // Одному часу соответствует 30 градусов.
            var deltaN = Math.Abs(6 - n);
            Console.WriteLine(180 - deltaN * 30);
            Console.ReadKey();
        }
    }
}
