﻿using System;

namespace ConsoleApp1
{
    class Loops5
    {

        static void Main(string[] args)
        {
            // Дана строка из символов '(' и ')'. Определить, является ли она корректным скобочным выражением. 
            Console.WriteLine("Введите скобочки в произвольном порядке: ");
            var str = Console.ReadLine();

            var result = 0;

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == '(')                
                    result++;
                else
                        result--;
                
            }

            if (result == 0)
                Console.WriteLine("Это корректное скобочное выражение");
            else
                Console.WriteLine("Это некорректное скобочное выражение");

        }

    }
}

