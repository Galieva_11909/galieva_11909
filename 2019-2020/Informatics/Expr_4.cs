﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Seminarskie
{
    class Expr_4
    {

        static void Main(string[] args)
        {
            //Найти количество чисел меньших N, которые имеют простые делители X или Y.
            var x = int.Parse(Console.ReadLine());
            var y = int.Parse(Console.ReadLine());
            var n = int.Parse(Console.ReadLine());
            var divOnX = n / x;
            var divOnY = n / y;
            var divOnXandY = n / (x * y);
            Console.WriteLine(divOnX + divOnY - divOnXandY);
        }
    }
}
