﻿using System;
using System.Collections.Generic;

namespace Game
{
    public static class RandomExtensions
    {
        public static int NextDouble(this Random rnd, int min, int max)
        {
            return (int)(rnd.NextDouble() * (max - min) + min);
        }
    }
    public class Gamer
    {
        public int GamerSum;
        public int ComputerSum;
    }

    class Program // Логика игры
    {
        public static void Play(Dictionary<int, int> counter, Random x, int sum, int n, bool who) // Вместо sum будет либо GamerSum, либо ComputerSum
        {
            while (sum < 21)
            {
                n = x.NextDouble(6, 10); // Игроку выпадает какая-то рандомная карта, в n всегда лежит то число, которое нужно прибавить
                var tuz = x.Next(0, 2); // Генерация для туза, 1 = Выпал туз, 0 - не туз
                if (tuz == 1 && who == true) // Если ход человека, то даем ему возможность выбрать между 1 и 11
                {
                    Console.WriteLine("Игрок, выберите число 1 или 11: ");
                    n = int.Parse(Console.ReadLine());
                }
                else if (tuz == 1 && who == false) // Если ход компьютера, то он выбирает рандомное число.
                {
                    if (!counter.ContainsKey(11)) // Проверяем, встречались ли до этого тузы, если нет, создаем для них пару ключ-значение
                        counter[11] = 0;
                    if (!counter.ContainsKey(1))
                        counter[1] = 0;
                    if (n > 6 && counter[11] < 4) // Если до этого туз выпал уже 4 раза, то опускаем.
                        n = 11;
                    else if (counter[1] < 4)
                        n = 1;
                }
                if (counter.ContainsKey(n))
                {
                    if (n == 10 && counter[10] != 13) // Всего карт с ценой "10" 13 штук, если 
                    {
                        counter[n]++;
                        sum += n;
                        break;
                    }
                    else if (n == 10 && counter[10] == 13) // Если их больше 13, значит, все карты с ценой "10" уже выходили, поэтому генерируем карту заново
                        continue; 
                    else
                    {
                        if (counter[n] != 4) // Если выпала карта не ценой "10", снова проверяем, сколько раз до этого она встречалась (если меньше 4, то все хорошо)
                        {
                            counter[n]++;
                            sum += n;
                            break;
                        }
                        else
                            continue; // Если больше 4, то снова генерируем карту.
                    }
                }
                else
                {
                    counter[n] = 1;
                    sum += n;
                    break;
                }
            }
        }

        static void Main(string[] args)
        {

            var counter = new Dictionary<int, int>(); // Ключ - карта, значение - количество раз, которое она сгенерировалась
            var x = new Random(); // Случайно сгенерирвоанное карта из колоды
            int n = 0; // Случайно сгенерирвоанное карта из колоды        
            Gamer player_1 = new Gamer(); //Ччеловек
            Gamer player_2 = new Gamer(); // кОмпьютер
            var who = true; // true = пользователь,  false = компьютер

            while (player_1.GamerSum < 21 && player_2.ComputerSum < 21)
            {
                if (who == true)
                {
                    Play(counter, x, player_1.GamerSum, n, who);
                }
                else
                {
                    Play(counter, x, player_2.GamerSum, n, who);
                }
                who = !who; // Меняем очередь
            }
            if (player_1.GamerSum < player_2.ComputerSum)
                Console.WriteLine("Вы проиграли");
            else if (player_1.GamerSum > player_2.ComputerSum)
                Console.WriteLine("Вы выиграли");
            else
                Console.WriteLine("Ничья");
        }
    }
}
