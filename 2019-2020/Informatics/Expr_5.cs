﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Seminarskie
{
    class Expr_5
    {
        static void Main(string[] args)
        {
            //Найти количество високосных лет на отрезке [a, b] не используя циклы.
            Console.WriteLine("Введите а: ");
            var a = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите b: ");
            var b = int.Parse(Console.ReadLine());

            var numberOnB = (b / 4 - b / 100) + b / 400;
            var numberOnA = (a / 4 - a / 100) + a / 400;
            Console.WriteLine("Общее количество равно: {0}", numberOnB - numberOnA + 1);
        }

    }
}
