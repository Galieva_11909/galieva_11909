﻿using System;

namespace ConsoleApp1
{
    class Expr12
    {
        static void Main(string[] args)
        {
            // h , t , v и x 
            Console.WriteLine("ВВедите h , t , v и x : ");
            var k = Console.ReadLine();
            var values = k.Split(" ");
            var h = double.Parse(values[0]);
            var t = double.Parse(values[1]);
            var v = double.Parse(values[2]);
            var x = double.Parse(values[3]);

            var min = 0.0;
            var max = 0.0;

            var tx = t * x;

            if (tx >= h)
            {
                min = 0;
                max = h /x;
            }
            else
            {
                min = (h - x * t ) / (v - x);
                max = t;
            }
            Console.WriteLine("{0}, {1}", min, max);

        }
    }
}
