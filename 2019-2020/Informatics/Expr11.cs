﻿using System;

namespace ConsoleApp1
{
    class Expr11
    {
        static void Main(string[] args)
        {
            //Expr11.Дано время в часах и минутах. Найти угол от часовой к минутной стрелке на обычных часах.

            Console.WriteLine("Введите часы: ");
            var hours = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите минуты:  ");
            var min = int.Parse(Console.ReadLine());

            if (hours > 12)
            {
                hours = hours - 12;
            }

            // Одному часу соответствует 30 градусов.
            var deltaH = Math.Abs(6 - hours);
            hours = 180 - deltaH * 30;

            //Одной минуте соответствует 6 градусов
            var deltaM = Math.Abs(30 - min);
            min = 180 - deltaM * 6;

            Console.WriteLine("Угол между стрелками: {0}", Math.Abs(Math.Min(hours, min) - Math.Max(hours, min)));


            
        }
    }
}
