﻿using System;

namespace SeminarTasks
{
    class Expr_10
    {
        public static int SumOfArithmeticProgression(int n)
        {
            var lastNumber = (1000 / n) * n;
            if (lastNumber == 1000)
            {
                lastNumber = 1000 - n;
            }
            return (n + lastNumber) / 2 * (1000 / n);
        }
        


        static void Main(string[] args)
        {
            //Найти сумму всех положительных чисел меньше 1000 кратных 3 или 5.
            Console.WriteLine("Сумма всех положительных чисел, меньших 1000 и кратных 3 или 5: {0} ", SumOfArithmeticProgression(3) + SumOfArithmeticProgression(5) - SumOfArithmeticProgression(15));                                   
        }
    }
}
