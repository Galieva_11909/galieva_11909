﻿using System;

namespace SeminarTasks
{
    class Expr_6
    {
        //Посчитать расстояние от точки до прямой, заданной двумя разными точками.
        public static double GetLength(double x1, double x2, double y1, double y2)
        {
            return Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));// вычисление длины прямой с данными координатыми
        }
        public static void FillInArray(double[,] array)
        {
            for (int y = 0; y <= 1; y++)
            {
                if (y == 0)
                {
                    Console.WriteLine("Введите x: ");
                    array[0, y] = double.Parse(Console.ReadLine());
                }
                else
                {
                    Console.WriteLine("Введите у: ");
                    array[0, y] = double.Parse(Console.ReadLine());
                }

            }
        }
        static void Main(string[] args)
        {
            var firstPoint = new double[1, 2];
            var secondPoint = new double[1, 2];
            var point = new double[1, 2];
            Console.WriteLine("Введите координаты первой точки прямой: ");
            FillInArray(firstPoint);
            Console.WriteLine("Введите координаты второй точки прямой: ");
            FillInArray(secondPoint);
            Console.WriteLine("Введите координаты точки : ");
            FillInArray(point);

            var hypotenuse = GetLength(firstPoint[0, 0], secondPoint[0, 0], firstPoint[0, 1], secondPoint[0, 1]);
            var firstCathet = GetLength(firstPoint[0, 0], point[0, 0], firstPoint[0, 1], point[0, 1]);
            var secondCathet = GetLength(secondPoint[0, 0], point[0, 0], secondPoint[0, 1], point[0, 1]);

            Console.WriteLine("Расстояние от точки до прямой равно: {0}", (firstCathet * secondCathet) / hypotenuse);

        }
    }
}
