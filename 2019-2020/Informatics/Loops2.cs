﻿using System;

namespace ConsoleApp1
{
    class Loops2
    {

        static void Main(string[] args)
        {

            Console.WriteLine("Введите число N: ");
            var N = int.Parse(Console.ReadLine());

            var result = 0;
            for (int i = 100; i<1000; i++)
            {
                var str = i.ToString();
                if (Convert.ToInt32(str[0] - '0') + Convert.ToInt32(str[1] - '0') + Convert.ToInt32(str[2] - '0') == N)
                    result++;
            }
            Console.WriteLine("{0}", result);
           
        }

    }
}

