﻿using System;

namespace ConsoleApp1
{
    class Cond1
    {
        public static int ToMatchLetter(char letter)
        {
            if (letter == 'A')
                return 0;
            else if (letter == 'B')
                return 1;
            else if (letter == 'C')
                return 2;
            else if (letter == 'D')
                return 3;
            else if (letter == 'E')
                return 4;
            else if (letter == 'F')
                return 5;
            else if (letter == 'G')
                return 6;
            else
                return 7;
        }
        static void Main(string[] args)
        {

            Console.WriteLine("Введите начальные координаты: ");
            var k = Console.ReadLine();
            var y1 = k[1] - 1;
            var x1 = ToMatchLetter(k[0]);

            Console.WriteLine("Введите конечные координаты: ");
            k = Console.ReadLine();
            var y2 = k[1] - 1;
            var x2 = ToMatchLetter(k[0]);

            if (Math.Abs(x1 - x2) == Math.Abs(y1 - y2) || x1 == x2 || y1 == y2)
                Console.WriteLine("Корректный ход для ферзя");
            else
                Console.WriteLine("Некорректный ход для ферзя");

            if (Math.Abs(x1 - x2) == 1 || Math.Abs(y1 - y2) == 1)
                Console.WriteLine("Корректный ход для короля");
            else
                Console.WriteLine("Некорректный ход для короля");

            if (Math.Abs(x1 - x2) != 0 && y1 == y2 || x1 == x2 && Math.Abs(y1 - y2) != 0)
                Console.WriteLine("Корректный ход для ладьи");
            else
                Console.WriteLine("Некорректный ход для ладьи");

            if (Math.Abs(x1 - x2) == Math.Abs(y1 - y2))
                Console.WriteLine("Корректный ход для слона");
            else
                Console.WriteLine("Некорректный ход для слона");

            if (Math.Abs(x1 - x2) != 0 && Math.Abs(y1 - y2) == 1 || (Math.Abs(x1 - x2) == 1 && Math.Abs(y1 - y2) != 0))
                Console.WriteLine("Корректный ход для коня");
            else
                Console.WriteLine("Некорректный ход для коня");



        }


    }
}

