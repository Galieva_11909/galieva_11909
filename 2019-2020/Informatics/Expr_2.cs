﻿using System;

namespace Seminarskie
{
    class Expr_2
    {
       /static void Main(string[] args)
        {
            //Задается натуральное трехзначное число (гарантируется, что трехзначное). 
            //Развернуть его, т.е. получить трехзначное число, записанное теми же цифрами в обратном порядке.

            //Работает для числа любого разряда
            var number = Console.ReadLine();
            for (int i = number.Length - 1; i >= 0; i--)
                Console.Write(number[i]);

        }
    }
}
