/*1. Написать запрос для подсчёта факториала с помощью рекурсивного запроса. Вывести первые 19 строк.*/

WITH RECURSIVE temp (i, factorial) AS 
(SELECT 0, 1 
    UNION ALL 
SELECT i+1, (i+1)*factorial 
FROM temp WHERE i <= 19)
SELECT * FROM temp;

/*2. Создать таблицу с географической информацией со следующими полями 
	(id, par_id, name)
   Со значениями:
	(1, null, 'Планета Земля'),
	(2, 1, 'Континент Евразия'),
	(3, 1, 'Континент Северная Америка'),
	(4, 2, 'Европа'),
	(5, 4, 'Россия'),
	(6, 4, 'Германия'),
	(7, 5, 'Москва'),
	(8, 5, 'Санкт-Петербург'),
	(9, 6, 'Берлин');
   Получить все объекты из Европы с уровнем вложенности.*/


WITH RECURSIVE t AS 
   (SELECT id, par_id, name, 1 AS level
   FROM geogr
   WHERE name = 'Европа'

   UNION ALL

   SELECT geogr.id, geogr.par_id, geogr.name, t.level + 1 AS level
   FROM geogr
      JOIN t
          ON geogr.par_id = t.id)
SELECT name, level FROM t;


/* НЕ ДЕЛАТЬ с материал. представлениями
3. Переписать запросы 5 и 8 с помощью временных таблиц. Проверить, что таблицы будут удалены, после закрытия сессии.

4. Создать два вида представления, одно materilzed, другое нет, по следующему запросу:
	Отобрать всех режиссеров мужского пола, которые снимали фильмы с бюджетом более 1000000.
	Сменить пол у некоторых режиссеров, проверить, что произойдёт с представлениями.
5. Использовать представление из предыдущего запроса для извлечения всех фильмов, которые снимались режиссёрами из представления.*/

/*6. Получить года, в которых было снято 3 фильма с бюджетом более 300000.*/
SELECT movies_with_budg_more_30000.year
  FROM movies,
(SELECT *
          FROM movies
         WHERE movies.budget>300000) AS movies_with_budg_more_30000
GROUP BY (movies_with_budg_more_30000.year)
HAVING (COUNT(movies_with_budg_more_30000.year) =3);

/*7. Получить режиссеров, которые сняли хотя бы один фильм, в котором снялось более 5 актеров*/
WITH movies_with_5_actors AS(
	SELECT movies_actors.movie_id 
	FROM movies_actors 
		JOIN movies 
		ON (movies.movie_id = movies_actors.movie_id)
	GROUP BY (movies_actors.movie_id)
	HAVING (COUNT( movies_actors.movie_id)>5)
)
SELECT producer.name FROM producer, movies_producers, movies_with_5_actors
WHERE (movies_with_5_actors.movie_id = movies_producers.movie_id AND movies_producers.producer_id = producer.id);


/*8. Получить актеров, которые снялись в фильмах в количестве от 3 до 7.*/
WITH actors_id AS(
	SELECT actors.id FROM movies_actors, actors
	WHERE (actors.id = movies_actors.actor_id)
	GROUP BY (movies_actors.actor_id, actors.id)
	HAVING(COUNT(movies_actors.actor_id) <= 7 AND COUNT(movies_actors.actor_id) >=3 ))
SELECT actors.name FROM actors_id, actors
WHERE (actors.id = actors_id.id);

	
