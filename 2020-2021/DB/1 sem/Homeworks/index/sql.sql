CREATE INDEX name_ind
ON actors (name);

CREATE INDEX surname_idx ON actors (surname) WHERE (surname SIMILAR TO '%v');

CREATE INDEX motherland_ind
ON actors (motherland) WHERE (motherland = 'USA');

CREATE INDEX order_idx ON actors (number_of_movies ASC);