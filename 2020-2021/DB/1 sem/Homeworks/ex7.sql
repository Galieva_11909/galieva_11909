/*Заполняю таблицу данными, удаляю лишние строки*/
UPDATE producer SET best_film_id = 2 WHERE id = 1;
UPDATE producer SET best_film_id = 3 WHERE id = 2;
UPDATE producer SET best_film_id = 5 WHERE id = 3;
UPDATE producer SET best_film_id = 4 WHERE id = 4;
UPDATE producer SET best_film_id = 1 WHERE id = 5;
UPDATE producer SET best_film_id = 6 WHERE id = 6;
DELETE FROM producer WHERE id = 7;
ALTER TABLE products DROP COLUMN description;
INSERT INTO movies_producers (movie_id,producer_id) VALUES
(1, 2),
(3, 2),
(5, 3),
(4, 4),
(1, 5),
(6,6),
(1, 3),
(1, 4),
(1, 6),
(1,1);
INSERT INTO movies_producers (producer_id, movie_id) VALUES
(1, 2),
(3, 2),
(5, 3),
(1, 5),
(1, 3),
(1, 4),
(1, 6);

INSERT INTO movies_actors (movie_id, actor_id) VALUES
(1, 2),
(3, 2),
(5, 3),
(4, 4),
(1, 5),
(6,6),
(1, 3),
(1, 4),
(1, 6),
(1,1);


/*1. Отобрать всех режиссёров, у которых лучший фильм был снят в 2000 году. */
SELECT producer.name, producer.surname, producer.birthday, producer.motherland FROM producer, movies WHERE producer.best_film_id = movies.movie_id AND movies.year = 2000;

/*2. Вывести всех режиссёров, которые сняли более 5 фильмов.*/
SELECT  producer.name, producer.surname, producer.birthday, producer.motherland
FROM producer, movies_producers
WHERE (movies_producers.producer_id = producer.id)
GROUP BY (producer.name, producer.surname, producer.birthday, producer.motherland)
HAVING COUNT(movies_producers.producer_id)>5;

/*3. Отобрать илентификаторы фильмов, где снималось более 5 актёров.*/
SELECT movies_actors.movie_id FROM movies, movies_actors
WHERE (movies_actors.movie_id = movies.movie_id)
GROUP BY (movies_actors.movie_id)
HAVING COUNT(movies_actors.movie_id)>5;

/*4. Добавить поле оценка в таблицу фильмов. Получить топ-3 фильма с наивысшей оценкой, снятых в США.*/
ALTER TABLE movies ADD COLUMN mark integer;
/*Заполняю таблцицу*/
UPDATE movies SET mark = 10 WHERE movie_id = 1;
UPDATE movies SET mark = 3 WHERE movie_id = 2;
UPDATE movies SET mark = 9 WHERE movie_id = 3;
UPDATE movies SET mark = 10 WHERE movie_id = 4;
UPDATE movies SET mark = 8 WHERE movie_id = 5;
UPDATE movies SET mark = 5 WHERE movie_id = 6;
/*Запрос:*/
SELECT name, mark, country FROM movies
WHERE (movies.country = 'USA')
ORDER BY (mark) DESC
LIMIT 3;

/*5. Отобрать все различные фильмы фантастика, в которых снимались актёры родом из США.*/

/*Зfполняю таблицу*/
INSERT INTO movies_genres (movie_id, genre_id) VALUES
(1, 1),
(2, 2,),
(3,1),
(4, 3),
(5, 2),
(6, 4);

SELECT DISTINCT movies.name FROM movies, movies_genres, actors, movies_actors 
WHERE (actors.motherland = 'USA' 
	   AND movies_genres.genre_id = 1 /*1 - фантастика*/
	   AND movies_genres.genre_id = movies.movie_id 
	   AND movies.movie_id = movies_actors.movie_id
	   AND movies_actors.actor_id = actors.id);

/*6. Отобрать все жанры фильмов, по которым в базе данных присутствует ровно 2 фильма.*/
SELECT genres.name FROM genres, movies_genres
WHERE (genres.id = movies_genres.genre_id)
GROUP BY (genres.name)
HAVING COUNT(movies_genres.movie_id) = 2;

/*7. Получить вторую тройку фильмов по оценкам.*/
SELECT name FROM movies
ORDER BY (mark)
LIMIT 3;

/*8. Вывести жанры фильмов. которые снимал режиссер из Англии или из Франции */
SELECT DISTINCT genres.name FROM genres, movies_genres, movies_producers, producer
WHERE (genres.id = movies_genres.genre_id 
	  AND movies_genres.movie_id = movies_producers.movie_id
	  AND movies_producers.producer_id = producer.id
	  AND (producer.motherland = 'USA' OR producer.motherland = 'France'));

