SELECT*FROM movies;
CREATE SEQUENCE order_of_movies;
ALTER TABLE movies ADD movie_id integer DEFAULT nextval('order_of_movies');

//1. Организовать связи между таблицами, добавив недостающие поля в таблицы (на один фильм может приходится множество актеров и режиссеров).
ALTER TABLE movies ADD COLUMN producer_id integer;
ALTER TABLE producer ADD PRIMARY KEY (id);

CREATE TABLE movies_producers 
(
	movie_id integer,
	producer_id integer,
	PRIMARY KEY (movie_id, producer_id)
);

ALTER TABLE movies_producers 
ADD CONSTRAINT connect_movie_and_producers FOREIGN KEY (movie_id)
REFERENCES movies (movie_id) MATCH SIMPLE;

ALTER TABLE movies_producers 
ADD CONSTRAINT connect_producers_and_movie FOREIGN KEY (producer_id)
REFERENCES producer (id) MATCH SIMPLE;

ALTER TABLE actors ADD PRIMARY KEY (id);
CREATE TABLE movies_actors 
(
	movie_id integer,
	actor_id integer,
	PRIMARY KEY (movie_id, actor_id)
);

ALTER TABLE movies_actors 
ADD CONSTRAINT connect_movie_and_actors FOREIGN KEY (movie_id)
REFERENCES movies (movie_id) MATCH SIMPLE;

ALTER TABLE movies_actors 
ADD CONSTRAINT connect_actors_and_movie FOREIGN KEY (actor_id)
REFERENCES actors (id) MATCH SIMPLE;

//2. Добавить в таблицу режиссёров связь с их лучшим фильмом в таблице фильмы. Подумать как бы это пришлось делать, если бы ссылка предполагалась изначально.
ALTER TABLE producer ADD COLUMN best_film_id integer;
ALTER TABLE producer ADD CONSTRAINT best_movie_id FOREIGN KEY (best_fil_id)
REFERENCES movies (movie_id) MATCH SIMPLE

//3. Поменять для таблицы фильмы первичный ключ: добавить новое поле(movie_id) и соответстувенно новую последовательность для этого поля.
ALTER TABLE movies
ADD CONSTRAINT key_of_movies
PRIMARY KEY (movie_id);

//4. Изменить значение по умолчанию для поля Страна на "UK".
ALTER TABLE movies ALTER COLUMN country SET DEFAULT 'USA';

//5. Удалить ограничение на число фильмов для актеров.
ALTER TABLE actors DROP CONSTRAINT range_of_numbers_of_movies;

//6. Поменять ограничение на бюджет фильма: поле бюджет не должен быть < 1000. 
ALTER TABLE movies DROP CONSTRAINT range_of_budget;
ALTER TABLE movies ADD CONSTRAINT range_of_budget CHECK(budget >= 1000);

//7. Выделить жанры в отдельную таблицу, организовать межтабличную связь.
CREATE TABLE genres
(
	id integer PRIMARY KEY,
	name text
);

CREATE TABLE movies_genres
(
	movie_id integer,
	genre_id integer,
	CONSTRAINT movies_genres_key PRIMARY KEY (movie_id, genre_id),	
	CONSTRAINT movies_genres FOREIGN KEY (genre_id)
	REFERENCES genres (id) MATCH SIMPLE,
	CONSTRAINT genres_movies FOREIGN KEY (movie_id)
	REFERENCES movies (movie_id) MATCH SIMPLE	
);

//8. Изменить тип для поля страна рождения. Сделать его перечислением из следующих вариантов ("USA", "UK", "Russia", "France", "Germany")
CREATE TYPE country AS ENUM ('USA', 'UK', 'Russia', 'France', 'Germany');


//9. Добавить проверку на поле дата рождения: она не должна превышать текущую дату.
ALTER TABLE actors ADD CONSTRAINT check_not_today CHECK (CURRENT_DATE >= birthday);

//10 Создать представлеиие, которое будет возваращать информацию об актераз, которые снимались в фильмах, которые вышли после 2000 года.
CREATE VIEW actors_after_2000
 AS SELECT actors.id,
 actors.surname,
 actors.name,
 actors.birthday,
 actors.motherland,
 actors.number_of_movies
 FROM 
 actors, movies_actors, movies
WHERE movies.year >2000 AND movies_actors.actor_id = actor_id 
AND movies_actors.movie_id = movies.movie_id
ORDER BY actors.id;
 
