/*1. Отобрать всех режиссёров, у которых лучший фильм был снят в 2000 году. */
SELECT producer.name, producer.surname
FROM producer, movies WHERE producer.best_film_id = movies.movie_id AND movies.year = 2000;
/*или:*/
SELECT producer.name, producer.surname
FROM producer JOIN movies ON producer.best_film_id = movies.movie_id WHERE movies.year = 2000;

/*2. Вывести всех режиссёров, которые сняли более 5 фильмов.*/
SELECT  producer.name, producer.surname, producer.birthday, producer.motherland
FROM producer, movies_producers
WHERE (movies_producers.producer_id = producer.id)
GROUP BY (producer.name, producer.surname, producer.birthday, producer.motherland)
HAVING COUNT(movies_producers.producer_id)>5;

/*3. Отобрать идентификаторы фильмов, где снималось более 5 актёров*/
SELECT movies_actors.movie_id
FROM movies JOIN movies_actors ON
movies_actors.movie_id = movies.movie_id
GROUP BY (movies_actors.movie_id)
HAVING COUNT(movies_actors.movie_id)>5;

/*4. Добавить поле оценка в таблицу фильмов. Получить топ-10 фильмов с наивысшей оценкой, снятых в США*/
SELECT name, mark, country FROM movies
WHERE (movies.country = 'USA')
ORDER BY (mark) DESC
LIMIT 3;


/*5. Отобрать все различные фильмы жанра фантастика, в которых снимались актёры родом из США*/

SELECT DISTINCT movies.name FROM movies 
JOIN movies_genres ON 
(movies_genres.movie_id = movies.movie_id AND movies_genres.genre_id = 1)
JOIN actors ON 	(actors.motherland = 'USA')
JOIN movies_actors ON (movies_actors.actor_id = actors.id AND movies.movie_id = movies_actors.movie_id );
	   
/*Вариант с NATURAL JOIN:*/
						
SELECT DISTINCT movies.name FROM movies 
NATURAL JOIN movies_genres
JOIN actors ON (actors.motherland = 'USA')
JOIN movies_actors ON (movies_actors.actor_id = actors.id AND movies.movie_id = movies_actors.movie_id )
WHERE (movies_genres.genre_id = 1);
	   
/*6. Отобрать актеров, которые снимались у режиссёров из Англии с 2007 по 2010 год*/

SELECT DISTINCT actors.name FROM actors 
JOIN movies_actors ON(actors.id = movies_actors.actor_id)
NATURAL JOIN movies_producers
JOIN movies ON ( movies.year >=2007 AND movies.year <= 2010 AND movies.movie_id = movies_producers.movie_id)
JOIN producer ON (producer.id =  movies_producers.producer_id AND producer.motherland = 'USA');


/*7. Оценить средние бюджеты фильмов, вышедших до 2000 года, с 2000 по 2005, с 2005 по 2010, с 2010 по настоящее время.*/
SELECT periods.movies_period, AVG(periods.budget) AS AVG_budget
FROM 
(SELECT
 		CASE
 		WHEN (movies.year<2000) THEN 'up_to_2000'	
 		WHEN (movies.year>= 2000 AND movies.year<2005 ) THEN 'from_2000_to_2005'	
		WHEN (movies.year>= 2005 AND movies.year<2010 ) THEN 'from_2005_to_2010'	
		WHEN (movies.year>= 2010) THEN 'after_2010'
 		END AS movies_period, budget 
	FROM movies
) AS periods
GROUP BY (periods.movies_period);

/*8. Получить суммарный бюджет фильмов, которы снимались режиссёрами, фамилия которых заканчивается на "V" или "N".*/
SELECT SUM(movies.budget) FROM movies_producers, movies,
(SELECT
 		CASE
 		WHEN (producer.surname SIMILAR TO '%v' ) THEN 'V'	
 		WHEN (producer.surname SIMILAR TO '%n') THEN 'N'		
 		END AS end_of_surname, id
	FROM producer
) AS right_producers
WHERE (right_producers.end_of_surname IS NOT NULL
	   AND right_producers.id = movies_producers.producer_id 
	   AND movies.movie_id = movies_producers.movie_id);


/*9. Получить максимальные бюджеты фильмов по годам*/
SELECT MAX(movies.budget), movies.year FROM movies GROUP BY (movies.year);

/*10. Получить все фильмы, снятые до 2010 года, бюджет которых меньше бюджета любого фильма снятого после 2010 года.*/

SELECT movies.name FROM movies,
(
	SELECT MIN(movies.budget) AS max_for_movies_before_2010 FROM movies 
	WHERE (movies.year >= 2010)
) AS budget
WHERE (movies.year < 2010 AND movies.budget < budget.max_for_movies_before_2010);

/*11. Получить режиссеров, которые сняли хотя бы один фильм бюджет которого больше минимума бюджета фильмов за 2015 год и меньше максимумального бюджета за 2016 год.*/
SELECT producer.name FROM producer, movies_producers, movies,
(
	/*минимум бюджета за 2015*/
	SELECT MIN(movies.budget) AS min FROM movies 
	WHERE (movies.year = 2015)
) AS year_2015,
(
	SELECT MAX(movies.budget) AS max FROM movies 
	WHERE (movies.year = 2016)
) AS year_2016
WHERE (producer.id =  movies_producers.producer_id
 AND movies_producers.movie_id = movies.movie_id 
 AND movies.budget > year_2015.min
 AND movies.budget <year_2016.max );


12. Получить список режиссеров, которые сняли хотя бы один фильи до 2000 года, или которые сняли всего ровно 10 фильмов.*/

SELECT producer.name FROM producer, movies_producers,
(
	SELECT movies.movie_id AS movies_id FROM movies
	WHERE (movies.year <=2000)
) AS movies_before_2000,
(
	SELECT movies_producers.producer_id AS producer_id FROM movies_producers
	GROUP BY (movies_producers.producer_id)
	HAVING (COUNT(movies_producers.movie_id) = 10)
) AS producers_with_10_movies
WHERE ((movies_producers.producer_id = producer.id 
		AND movies_producers.movie_id = movies_before_2000.movies_id) 
	   OR producers_with_10_movies.producer_id = producer.id)
;



