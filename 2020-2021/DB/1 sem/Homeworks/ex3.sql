/* Создание таблиц */
CREATE TABLE movies (
    name text,
    year integer CONSTRAINT range_of_years CHECK(year>1900 AND year<2020) ,
    did integer,
    genres text,
    country text,
    budget integer CONSTRAINT range_of_budget CHECK(budget >= 10000),
	CONSTRAINT key_of_movies PRIMARY KEY(name, year)
);

CREATE TABLE actors (
	numb integer,
    surname text,
    name text,
    birthday date,
    motherland text,
    number_of_movies integer CONSTRAINT range_of_numbers_of_movies CHECK (number_of_movies >5),
	CONSTRAINT must_be_only_one_actor UNIQUE (name, surname, birthday)
);

CREATE TABLE producer (
	numb integer,
    surname text,
    name text,
    birthday date,
    motherland text DEFAULT ('USA') 
);
/* Создание последовательностей */
CREATE SEQUENCE order_of_people OWNED BY actors.numb;
CREATE SEQUENCE order_of_producer OWNED BY producer.numb;

/* При создании таблиц я совершила ряд ошибок: 
1) указала неверное условие для года в таблице Movies
2) создала столбец с неправильным именем и типом
в следющих 4-х строка я исправляю ошибки.*/

ALTER TABLE movies DROP CONSTRAINT range_of_years;
ALTER TABLE movies ADD CONSTRAINT range_of_years CHECK (year<2030);
ALTER TABLE movies RENAME COLUMN did TO description;
ALTER TABLE movies ALTER COLUMN description TYPE text;

/* Заполнение таблиц. */
INSERT INTO movies (name, description, year, genres, country, budget) VALUES 
	('Iron Man', 'American superhero film based on the Marvel Comics character of the same name.',
	2008, 'fantastic', 'USA', 1400000000),
	('The Judge', 'American legal drama film directed by David Dobkin.',
	2014, 'drama', 'USA', 45000000),
	('Dolittle', ' American fantasy adventure film directed by Stephen Gaghan from a screenplay by Gaghan, Dan Gregor, and Doug Mand, based on a story by Thomas Shepherd. ',
	2020, 'fantastic', 'USA', 175000000),
	('The Intouchables', 'French buddy comedy-drama film directed by Olivier Nakache & Éric Toledano. ',
	2011, 'comedy-drama', 'France', 95000000),
	('The Green Mile ', 'American fantasy drama film written and directed by Frank Darabont',
	1999, 'drama', 'USA', 60000000),
	('Sherlock Holmes', 'Action film based on the character of the same name created by Sir Arthur Conan Doyle. ',
	2009, 'detective', 'USA', 90000000);

INSERT INTO actors (surname, name, birthday, motherland, number_of_movies) VALUES 
	('Downey', 'Robert', '1965-04-04', 'USA', 244),
	('Pitt', 'Brad', '1963-12-18', 'USA', 298),
	('Jolie', 'Angelina', '1975-06-04', 'USA', 203),
	('Bortich', 'Alexandra', '1994-09-24', 'Belorussia', 39),
	('Petrov', 'Alexander', '1989-01-25', 'Russia', 69),
	('Burunov ', 'Sergey', '1977-03-06', 'Russia', 404);

INSERT INTO producer (surname, name, birthday, motherland) VALUES 
	('Spielberg', 'Steven', '1946-12-18', 'USA'),
	('Bondarchuk', 'Fyodor', '1967-05-9', 'Russia'),
	('Bekmambetov', 'Timur', '1961-05-25', 'Russia'),
	('Bruckheimer', 'Jerome', '1943-09-21', 'USA'),
	('Travolta', 'John', '1954-02-18', 'USA'),
	('Cameron', 'James', '1954-08-16', 'Canada');
	
	/* Удаление первичных ключей */
ALTER TABLE movies DROP CONSTRAINT key_of_movies;

/*Переименовываю столбцы с номерами */
ALTER TABLE actors RENAME COLUMN numb TO id;
ALTER TABLE producer RENAME COLUMN numb TO id;


/* Поняла, что неправильно создала последовательность, поэтому исправляю косяк:*/
ALTER TABLE producer DROP COLUMN id;
CREATE SEQUENCE order_of_producer;
ALTER TABLE producer ADD id integer DEFAULT nextval('order_of_producer');

ALTER TABLE actors DROP COLUMN id;
CREATE SEQUENCE order_of_people;
ALTER TABLE actors ADD id integer DEFAULT nextval('order_of_people');

