﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace WebApplication3
{
    public class MyMiddlware
    {
        private readonly RequestDelegate _next;
        public MyMiddlware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var expression = context.Request.Query["input"];
            if (!String.IsNullOrEmpty(expression))
            {
                await context.Response.WriteAsync("Hello, " + expression);
            }
            else
            {
                await _next(context);
            }
        }
    }
}
