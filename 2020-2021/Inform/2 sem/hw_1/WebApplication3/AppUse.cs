﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace WebApplication3
{
    public static class AppUseCalculator
    {
        public static void UseMyMiddlware(this IApplicationBuilder app)
        {
          
            app.UseMiddleware<MyMiddlware>();
        }


    }
}
