using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.FileProviders;

namespace WebApplication1
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStaticFiles();
            app.UseRouting();
            //DefaultFilesOptions options = new DefaultFilesOptions();
            //options.DefaultFileNames.Clear(); // ������� ����� ������ �� ���������
            //options.DefaultFileNames.Add("index.html"); // ��������� ����� ��� �����
            //app.UseDefaultFiles(options); // ��������� ����������
            

            app.MapWhen(context =>
            {
                return context.Request.Query.ContainsKey("token") && context.Request.Query["token"] == "12345";
            }, Page);

            app.MapWhen(context =>
            {
                return context.Request.Query.ContainsKey("token") && context.Request.Query["token"] != "12345";
            }, Error);



           

          
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    var data = System.IO.File.ReadAllText("wwwroot/index.html");
                    await context.Response.WriteAsync(data);
                });
            });
        }

        private static void Page(IApplicationBuilder app)

        {
            app.Run(async context =>
            {
                var data = System.IO.File.ReadAllText("wwwroot/page.html");
                await context.Response.WriteAsync(data);
            });
        }

        private static void Error(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                var data = System.IO.File.ReadAllText("wwwroot/error.html");
                await context.Response.WriteAsync(data);
            });
        }
    }
}
