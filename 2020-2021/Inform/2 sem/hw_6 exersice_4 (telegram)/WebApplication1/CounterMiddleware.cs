﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace WebApplication1
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class CounterMiddleware
    {
        private readonly RequestDelegate _next;
        private static int counter;

        public CounterMiddleware(RequestDelegate next)
        {

            _next = next;
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        public Task Invoke(HttpContext httpContext)
        {
            var ip = GetLocalIPAddress();
            if (!httpContext.Request.Cookies.ContainsKey(ip));
            {
                httpContext.Response.Cookies.Append(ip, 1.ToString());
                counter++;
            }
            httpContext.Items.Add(0, "Number of unique users: " + counter);
            return _next(httpContext);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class CounterMiddlewareExtensions
    {
        public static IApplicationBuilder UseCounterMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CounterMiddleware>();
        }
    }
}
