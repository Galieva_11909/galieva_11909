﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class Middleware_3
    {
        private readonly RequestDelegate _next;

        public Middleware_3(RequestDelegate next)
        {
            _next = next;
        }

        public Task Invoke(HttpContext httpContext)
        {
            httpContext.Items.Add(3, "I'm in the result Middlware. I'm beginning output.");
            var result = new StringBuilder();
            foreach (var httpContextItem in httpContext.Items)
            {
                result.AppendLine((string) httpContextItem.Value);
            }
            return httpContext.Response.WriteAsync(result.ToString()); ;
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class Middleware_3Extensions
    {
        public static IApplicationBuilder UseMiddleware_3(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<Middleware_3>();
        }
    }
}
