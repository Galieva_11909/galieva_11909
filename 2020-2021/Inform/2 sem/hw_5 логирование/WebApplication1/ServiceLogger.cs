﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace WebApplication1
{
    public static class ServiceLogger
    {
        public static void LogServices(IServiceCollection services)
        {
            var data = System.IO.File.ReadAllLines("data.txt");
            foreach (var s in data)
            {
                switch (s)
                {
                    case "1":
                        services.AddSingleton<ILogger, ConsoleLogger>();
                        break;
                    case "2":
                        services.AddSingleton<ILogger, FileLogger>();
                        break;
                }
            }
        }
    }
}