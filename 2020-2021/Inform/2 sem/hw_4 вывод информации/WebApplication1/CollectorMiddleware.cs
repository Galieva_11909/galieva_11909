﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
 
namespace WebApplication1
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class CollectorMiddleware
    {
        private readonly RequestDelegate _next;

        public CollectorMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public Task Invoke(HttpContext httpContext)
        {
            var userAgent = httpContext.Request.Headers["User-Agent"].ToString();

            var os = String.Concat<char>(userAgent.Split()[1].Skip(1));
            var full = userAgent.IndexOf("Chrome");
            var browserFull = String.Concat<char>(userAgent.Skip(full)).Split()[1];
            var browserIndex = browserFull.IndexOf('/');
            var browser = String.Concat<char>(browserFull.Take(browserIndex));

            if (!Collector.browsersCounter.ContainsKey(browser))
            {
                Collector.browsersCounter.Add(browser, 1);
            }
            else
            {
                Collector.browsersCounter[browser]++;
            }

            if (!Collector.OScounter.ContainsKey(os))
            {
                Collector.OScounter.Add(os, 1);
            }
            else
            {
                Collector.OScounter[os]++;
            }
            return _next(httpContext);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class CollectorMiddlewareExtensions
    {
        public static IApplicationBuilder UseCollectorMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CollectorMiddleware>();
        }
    }
}
