﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1
{
    public static class Collector
    {
        public static Dictionary<string, int> browsersCounter = new Dictionary<string, int>();
        public static Dictionary<string, int> OScounter = new Dictionary<string, int>();

        public static string GetAllData()
        {
            var text = new StringBuilder();
            text.AppendLine("Browser:");
            foreach (var pair in browsersCounter)
            {
                text.AppendLine(pair.Key + " : " + pair.Value);
            }
            text.AppendLine("OS:");
            foreach (var pair in OScounter)
            {
                text.AppendLine(pair.Key + " : " + pair.Value);
            }

            return text.ToString();
        } 
        // public static Dictionary<string, int> browsersCounter;

    }
}
