var d = document;


var number;
var name;
var surname;
var patronymic;
var birthday;
var group;
var mail;

function check(column, val) {	
	switch(column) {
		case(0):
		{
			var re=/[0-9]*/;
			return re.test(String(val));
		}
		case(1):
		{
			var re=new RegExp("[A-Za-zА-Яа-яЁё]{3,}");
			return re.test(String(val));
		}
		case(2):
		{
			var re=new RegExp("[A-Za-zА-Яа-яЁё]{3,}");
			return re.test(String(val));
		}
		case(3):		
		{
			var re=new RegExp("[A-Za-zА-Яа-яЁё]{3,}");
			return re.test(String(val));
		}
		case(4):
		{
			var re=/^\d{2}[./-]\d{2}[./-]\d{4}$/;
			return re.test(String(val));
		}		
		case(5):
		{
			 var re=/[0-9][0-9][-][0-9][0-9][0-9]/;
				return re.test(String(val));
		}
		case(6):
		{
			var re =/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(String(val).toLowerCase());
		}
	}
}

function addRow()
{    
	number = d.getElementById('number').value;
	surname = d.getElementById('surname').value;
    name = d.getElementById('name').value;	
	patronymic = d.getElementById('patronymic').value;
	birthday = d.getElementById('birthday').value;
	group = d.getElementById('group').value;
	mail = d.getElementById('mail').value;
	var isNeed = true;
	
	if (!check(0, number)) 
	{
		alert('Введите корректный номер студенческого');
		isNeed=false;
	}
	if (!check(1, surname))
	{	
		alert('Введите корректное имя');
		isNeed=false;
	}
	if (!check(2, name))
	{
		alert('Введите корректную фамилию');
		isNeed=false;
	}
	if (!check(3, patronymic))
	{
		alert('Введите корректное отчество');
		isNeed=false;
	}
	if (!check(4, birthday))
	{
		
		alert('Введите корректную дату рождения');
		isNeed=false;
	}
	if (!check(5, group))
	{
		alert('Введите корректный номер группы');
		isNeed=false;
	}
	if (!check(6, mail))
	{		
		alert('Введите корректную почту');
		isNeed=false;
	}
	
	if (isNeed == true)
	{

	var tbody = d.getElementById('tab1').getElementsByTagName('TBODY')[0];    
    var row = d.createElement("TR");
	
    tbody.appendChild(row);    
	
    var td1 = d.createElement("TD");
    var td2 = d.createElement("TD");
	var td3 = d.createElement("TD");
    var td4 = d.createElement("TD");
	var td5 = d.createElement("TD");
    var td6 = d.createElement("TD");
	var td7 = d.createElement("TD");
	
    row.appendChild(td1);
    row.appendChild(td2);
	row.appendChild(td3);
    row.appendChild(td4);
	row.appendChild(td5);
    row.appendChild(td6);
	row.appendChild(td7);

    td1.innerHTML = number;
    td2.innerHTML = surname;
	td3.innerHTML = name;
    td4.innerHTML = patronymic;
	td5.innerHTML = birthday;
    td6.innerHTML = group;
	td7.innerHTML = mail;   
	
	}
}

function searchNeedStudent(criterion) {
	let numberOfColumn;
	let beginStr;
	switch (criterion) {
		case('patronymic'):
		numberOfColumn = 3;
		beginStr = d.getElementById('patronymicSearch').value;
		break;
		case('name'):
		numberOfColumn = 2;
		beginStr = d.getElementById('nameSearch').value;	
		break;
		case('surname'):
		numberOfColumn = 1;
		beginStr = d.getElementById('surnameSearch').value;
		break;
	}
	let rows = Array.from(tab1.rows).slice(3);
	outputNeedStudents(rows, beginStr, numberOfColumn);
}

var allStudents;
var needStudents = new Array();
function outputNeedStudents(rows, beginStr, numberOfColumn) {
	var length = beginStr.length;
	allStudents = rows;
	for (let i = allStudents.length-1; i >=0; i--) {
		let curr = rows[i].cells[numberOfColumn].innerHTML.slice(0,length);
		if (beginStr != curr)
		{
			tab1.tBodies[0].deleteRow(i);
		}
	}
	isAll = false;
	
}

var isAll = true;
function returnStud() {
	if (isAll == false)
	{
		for (let i = 0; i < allStudents.length; i++) {
			tab1.tBodies[0].append(allStudents[i]);
		}
	}
	
}
function sort(number) {
	let sortedRows = Array.from(tab1.rows)
	.slice(3)
	.sort((rowA, rowB) => rowA.cells[number].innerHTML > rowB.cells[number].innerHTML ? 1 : -1);

	tab1.tBodies[0].append(...sortedRows);
}