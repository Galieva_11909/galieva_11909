﻿using System;
using System.Data.SqlClient;
using System.Linq;
using Npgsql;

namespace PostgresADO
{
	class Program
	{
		//Написать программу, которая будет подключаться к БД фильмография с помощью ADO.NET.
		//Позволит просматривать список таблиц в БД,
		// отображать содержимое отдельно взятой Таблицы, 
		// добавлять в неё проиpзвольные данные, 
		// при этом проверяя корректность ввода и вывода ошибку в случае проблем. 
		// 	Сделать возможность фильтрации данных по явному условию whereString. Учитывать SQL-инъекции

		//здесь все спец. сиволы,которые запрещены!
		private static char[] specSymbols = new char[] {'*', '/', '+' };
		
		
		//Получает команду, Позволяющую просматривать список таблиц в БД,
		private static NpgsqlCommand GetAllTables(NpgsqlConnection connection)
		{
			return new NpgsqlCommand(
				"SELECT table_name FROM information_schema.tables  WHERE table_schema='public' ORDER BY table_name",
				connection);
		}
		
		// Получает команду, Позволяющую отображать содержимое отдельно взятой Таблицы,
		private static NpgsqlCommand GetTable(string tableName, NpgsqlConnection connection)
		{
			foreach (var symbol in tableName)
			{
				if (specSymbols.Contains(symbol))
					throw new Exception("Некорректное имя таблицы!");
			}
			var cmd = new NpgsqlCommand("SELECT * FROM " + tableName, connection);
			cmd.ExecuteNonQuery();
			return cmd;
		}
		
		
		//Получает команду, Позволяющую добавлять в таблицу проиpзвольные данные, 
		private static NpgsqlCommand InsertInTable(string valuee, NpgsqlConnection connection)
		{
			//Имя таблицы задавать явно! 
			using (var cmd = new NpgsqlCommand("INSERT INTO  producers (col1) VALUES (@p)", connection))
			{
				cmd.Parameters.AddWithValue("p", valuee);
				cmd.ExecuteNonQuery();
			}

			return null;
		}
		
		static void Main(string[] args)
		{
			string connectionString = @"Server=127.0.0.1;Port=5432;Database=Filmography;User Id=postgres;Password=postgres;";

			
			using (NpgsqlConnection connection = new NpgsqlConnection(connectionString)) // подключаемся к бд
			{
				connection.Open();
				
				var c = GetAllTables(connection);
				var reader = c.ExecuteReader();
				
				if (reader.HasRows) // если есть данные
				{
					// выводим название столбца
					Console.WriteLine("{0}", reader.GetName(0));
					
					while (reader.Read())
					{
						object tableName = reader.GetValue(0);
						Console.WriteLine("{0}", tableName);
					}
				}
				reader.Close();
				
				reader = GetTable("movies",connection).ExecuteReader();
				if (reader.HasRows) // если есть данные
				{
					for (int i = 0; reader.GetName(i) != null; i++)
					{
						object str = reader.GetName(i);
						Console.Write("{0}", str);
					}
					Console.WriteLine();
					while (reader.Read())
					{
						for (int i = 0; reader.GetValue(i) != null; i++)
						{
							object str = reader.GetValue(i);
							Console.Write("{0}", str);
						}
						Console.WriteLine();
					}
				}
				reader.Close();
			}



		}
	}
}
