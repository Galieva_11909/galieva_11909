﻿using System;
using System.IO;
using System.Linq;


namespace selfHostedHttp
{
    public class Html
    {
        public static string GetHtml(string name)
        {
            var data = File.ReadAllLines(@$"html\{name}").Select(m=> String.Join("",m.Split("\t"))).ToArray();
            var text = String.Join("", data);
            return text;
        }
    }
}