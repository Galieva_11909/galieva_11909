﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace selfHostedHttp
{
    class Program
    {
        static void Main() => Listen().Wait();

        private static async Task Listen()
        {
            var bio = Html.GetHtml(@"bio.html");
            var contacts = Html.GetHtml(@"contacts.html");
            var cv = Html.GetHtml(@"CV.html");
            var photos = Html.GetHtml(@"photos.html");

            HttpListener listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:8080/bio/");
            listener.Prefixes.Add("http://localhost:8080/contacts/");
            listener.Prefixes.Add("http://localhost:8080/CV/");
            listener.Prefixes.Add("http://localhost:8080/photos/");
            listener.Start();
            while (true)
            {
                HttpListenerContext context = await listener.GetContextAsync();
                Task.Run(() =>
                {
                    HttpListenerResponse response = context.Response; // То, что выдадут пользователю на запрос.
                    string content;
                    switch (context.Request.Url.ToString())
                    {
                        case ("http://localhost:8080/bio/"):
                            content = bio;
                            break;
                        case ("http://localhost:8080/contacts/"):
                            content = contacts;
                            break;
                        case ("http://localhost:8080/CV/"):
                            content = cv;
                            break;
                        case ("http://localhost:8080/photos/"):
                            content = photos;
                            break;
                        default:
                            content = "Привет мир!";
                            break;
                    }
                    byte[] buffer = Encoding.UTF8.GetBytes(content);
                    response.ContentLength64 = buffer.Length;
                    response.OutputStream.Write(buffer, 0, buffer.Length);
                    response.OutputStream.Close();
                });
            }
        }
    }
}