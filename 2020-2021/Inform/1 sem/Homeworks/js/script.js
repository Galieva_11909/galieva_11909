var currentNumber = 0;
var operator = null;
var isFirst = true;
var isClear = true;

function addNumber(symb) {
	isFirst = false;
	var n = symb;
	var current = document.getElementById("result").value;
	document.getElementById("result").innerHTML  = current+n;
}

function calculate() {
	let second = Number(document.getElementById('result').value);
	let first = currentNumber;
	switch(operator) {
		case 'plus':
		currentNumber = first+second;
		break;
		
		case 'minus':
		currentNumber = first-second;
		break;
		
		case 'mult':
		currentNumber = first*second;
		break;
		
		case 'dev':
		if (second != 0) {
			currentNumber = first/second;
		}
		else 
			alert("На ноль делить нельзя!");
		break;
	}
	operator = null;
	document.getElementById('result').innerHTML = currentNumber;	
	
}

function cleanAll() {
	document.getElementById('result').innerHTML = "";
}
function doOperation(oper) {
	if (operator != null) {
		calculate();		
	}
	if (isFirst == false) {
		var number = document.getElementById('result');
		currentNumber = Number(number.value);		
		operator = oper;		
		document.getElementById('result').innerHTML = "";
		isFirst = true;
	}
}

