﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Net;
using System.Text;

namespace selfHostedHttp
{
	class Program
	{
		static void Main(string[] args)
		{
			var bio =  Html.GetHtml(@"bio.html");
			var contacts = Html.GetHtml(@"contacts.html");
			var CV = Html.GetHtml(@"CV.html");
			var photos = Html.GetHtml(@"photos.html");
			var head = Html.GetHtml(@"head.html");

			HttpListener listener = new HttpListener();
			listener.Prefixes.Add("http://localhost:8080/bio/");
			listener.Prefixes.Add("http://localhost:8080/contacts/");
			listener.Prefixes.Add("http://localhost:8080/CV/");
			listener.Prefixes.Add("http://localhost:8080/photos/");
			listener.Prefixes.Add("http://localhost:8080/head/");
			
			
			while (true)
			{
				listener.Start();
				HttpListenerContext context = listener.GetContext();
				HttpListenerRequest request = context.Request; // То, что вводит пользователь (запрос)
				HttpListenerResponse response = context.Response; // То, что ему выдадут на запрос.
				string content;
				
				// В зависимсоти от запроса, разный ответ
				switch (request.Url.ToString())
				{
					case ("http://localhost:8080/bio/"):
						content = bio;
						break;
					case ("http://localhost:8080/contacts/"):
						content = contacts;
						break;
					case ("http://localhost:8080/CV/"):
						content = CV;
						break;
					case ("http://localhost:8080/photos/"):
						content = photos;
						break;
					case ("http://localhost:8080/head/"):
						content = head;
						break;
					default:
						content = "Привет мир!";
						break;
				}
				byte[] bytes = Encoding.UTF8.GetBytes(content);
				response.ContentLength64 = bytes.Length;
				Stream sw = response.OutputStream;
				sw.Write(bytes, 0, bytes.Length);
				sw.Close();
			}
			listener.Stop();
			Console.WriteLine("Сервер закончил прослушивание порта 8080");
		}
	}
}
